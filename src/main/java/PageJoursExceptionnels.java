import Outils.PageOutils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

public class PageJoursExceptionnels extends PageOutils {

    public PageJoursExceptionnels(WebDriver driver) {
        super(driver);
    }

    // WebElement représentant l'onglet "Ressources"
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Ressources')]")
    WebElement onglet_Ressources;

    // WebElement représentant l'item "Calendrier"
    @FindBy(xpath = "//a[@href='/libreplan/calendars/calendars.zul']")
    WebElement item_calendrier;

    // WebElement représentant le titre de la page "Liste de calendriers"
    @FindBy(xpath = "//div[text() = 'Liste de calendriers']")
    WebElement title_calendrier;

    // WebElement représentant le boutton créer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer']")
    WebElement boutton_creer;

    // WebElement représentant le titre de colonne "Nom"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Nom')]")
    WebElement caledrier_nom;

    // WebElement représentant le titre de colonne "Hérité de la date"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Hérité de la date')]")
    WebElement caledrier_herite;

    // WebElement représentant le titre de colonne "Héritages à jour"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Héritages à jour')]")
    WebElement caledrier_heritages;

    // WebElement représentant le titre de colonne "Operations"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Opérations')]")
    WebElement caledrier_operations;

    // WebElement représentant le titre de la page "Modifier Calendrier : Calendrier - Test 1"
    @FindBy(xpath = "//td[text() = 'Modifier Calendrier: Calendrier - Test 1']")
    WebElement title_modifierTest1;

    // WebElement représentant le boutton "Enregistrer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer']")
    WebElement boutton_enregistrer;

    // WebElement représentant le boutton "Enregistrer et continuer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer et continuer']")
    WebElement boutton_sauver;

    // WebElement représentant le boutton "Annuler"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Annuler']")
    WebElement boutton_annuler;

    // WebElement représentant le champ Nom
    @FindBy(xpath = "//td[contains(., 'Nom')]/following-sibling::td//input")
    WebElement field_nom;

    // WebElement représentant l'icône "Modifier" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 1']/ancestor::tr//span[@title='Modifier']")
    WebElement icone_modifier;

    // WebElement représentant le bouton [Créer une exception]
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer une exception']")
    WebElement boutton_exception;

    // WebElement représentant le bouton [Mettre à jour l'exception]
    @FindBy(xpath = "//td[text() = \"Mettre à jour l'exception\"]")
    WebElement boutton_majException;

    // WebElement représentant le cadre du message d'erreur
    @FindBy(css = "div.z-errbox")
    WebElement cadre_error;
    // WebElement représentant la message d'exception KO
    @FindBy(xpath = "//div[@class='z-errbox-center']")
    WebElement message_exceptionKO;
    // WebElement représentant la croix du message d'exception KO
    @FindBy(xpath = "//div[@class='z-errbox-right z-errbox-close']")
    WebElement croix_exceptionKO;
    // WebElement représentant la fleche du message d'exception KO
    @FindBy(xpath = "//div[contains(@class, 'z-errbox-left')]")
    WebElement fleche_exceptionKO;

    // WebElement représentant le menu déroulant type d'exception
    @FindBy(xpath = "//td[text() = 'NO_EXCEPTION']/ancestor::i[@class='z-combobox']/i[@class='z-combobox-btn']")
    WebElement type_exception;

    // WebElement représentant le type d'exception "test_exception"
    @FindBy(xpath = "//td[normalize-space()='test_exception']")
    WebElement test_exception;

    // WebElement représentant le champ Date de fin
    @FindBy(xpath = "//td[contains(., 'Date de fin:')]/following-sibling::td//input")
    WebElement field_dateFin;

    // WebElement représentant la "Liste des exceptions"
    @FindBy(xpath = "//span[text() = 'Liste des exceptions']")
    WebElement exception_liste;

    // WebElement représentant le champ 2 "Date de début" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[1]//span")
    WebElement exceptionL_date2;

    // WebElement représentant le champ 2 "Type d'exception" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[2]//span")
    WebElement exceptionL_type2;

    // WebElement représentant le champ 2 "Effort normal" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[3]//span")
    WebElement exceptionL_effortN2;

    // WebElement représentant le champ 2 "Effort supplément" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[4]//span")
    WebElement exceptionL_effortS2;

    // WebElement représentant le champ 2 "Code" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[5]//input")
    WebElement exceptionL_code2;

    // WebElement représentant le champ 2 "Origine" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//tr[1]/td[6]//span")
    WebElement exceptionL_origine2;

    // WebElement représentant le tableau "Propriétés des jours"
    @FindBy(xpath = "//div[text() = 'Propriétés des jours']")
    WebElement exception_tableau;

    // WebElement représentant le champ correspondant à "Jour" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//input[@class='z-datebox-inp z-datebox-right-edge z-datebox-text-disd']")
    WebElement exceptionT_jour;

    // WebElement représentant le champ correspondant à "Type" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//tr[3]//tbody[2]/tr[2]/td[2]//span")
    WebElement exceptionT_type;

    // WebElement représentant le champ correspondant à "Temps travaillé" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//tr[3]//tbody[2]/tr[3]/td[2]//span")
    WebElement exceptionT_temps;

    // WebElement représentant le champ "Heures" correspondant à "Effort normal:"
    @FindBy(xpath = "//span[text()='Effort normal:']/../following-sibling::td[2]//i[@title='Heures']/input")
    WebElement effortN_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort normal:"
    @FindBy(xpath = "//span[text()='Effort normal:']/../following-sibling::td[2]//i[@title='Minutes']/input")
    WebElement effortN_minutes;

    // WebElement représentant le champ "Heures" correspondant à "Effort en heures supplémentaires:"
    @FindBy(xpath = "//span[text()='Effort en heures supplémentaires:']/../following-sibling::td[2]//i[@title='Heures']/input")
    WebElement effortS_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort en heures supplémentaires:"
    @FindBy(xpath = "//span[text()='Effort en heures supplémentaires:']/../following-sibling::td[2]//i[@title='Minutes']/input")
    WebElement effortS_minutes;

    // WebElement représentant l'item "Jours exceptionnels du calendrier"
    @FindBy(xpath = " //a[@href='/libreplan/excetiondays/exceptionDays.zul']")
    WebElement item_jourExceptionnel;

    // WebElement représentant le titre la page "Jours exceptionnels du calendrier Liste"
    @FindBy(xpath = "//div[text() = 'Jours exceptionnels du calendrier Liste']")
    WebElement title_jourEceptionnel;

    // WebElement représentant la colonne "Nom" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Nom')]")
    WebElement jourEception_nomT;

    // WebElement représentant la colonne "Couleur" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Couleur')]")
    WebElement jourEception_couleurT;

    // WebElement représentant la colonne "Sur-affecté" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Sur-affecté')]")
    WebElement jourEception_surAffecteT;

    // WebElement représentant la colonne "Effort standard" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Effort standard')]")
    WebElement jourEception_effortStandT;

    // WebElement représentant la colonne "Effort en heures supplémentaires" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Effort en heures supplémentaires')]")
    WebElement jourEception_effortSupT;

    // WebElement représentant la colonne "Opérations" dans le tableau
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Opérations')]")
    WebElement jourEception_operationsT;

    // WebElement représentant le titre de la page "Créer Jour du calendrier exceptionnel"
    @FindBy(xpath = "//td[text() = 'Créer Jour du calendrier exceptionnel']")
    WebElement title_creerJourExcep;

    // WebElement représentant l'onglet "Modifier"
    @FindBy(xpath = "//span[@class='z-tab-text' and text() = 'Modifier']")
    WebElement onglet_modifier;

    // WebElement représentant les champs du tableau d'exception
    @FindBy(xpath = "//div[@class='z-grid-body']//span[@class='z-label']")
    WebElement jourException_tableau;

    // WebElement représentant le champ de saisi correspondant à "Code"
    @FindBy(xpath = "//td[.//span[contains(., 'Code')]]/following-sibling::td//input[@type='text']")
    WebElement field_code;

    // WebElement représentant la " liste déroulante" correspondante au champ "Couleur"
    @FindBy(xpath = "//td[.//span[contains(., 'Couleur')]]/following-sibling::td//select")
    WebElement couleur_liste;

    // WebElement représentant la case "Couleur" correspondante à "Exception héritée"
    @FindBy(xpath = "//td[span[contains(., 'Exception héritée')]]/preceding-sibling::td[2]/div")
    WebElement couleur_caseHeri;

    // WebElement représentant la case "Couleur" correspondante à "Exception particulière"
    @FindBy(xpath = "//td[span[contains(., 'Exception particulière')]]/preceding-sibling::td/div")
    WebElement couleur_casePart;

    // WebElement représentant le champ "Heures" correspondant à "Effort standard"
    @FindBy(xpath = "//td[.//span[contains(., 'Effort stand')]]/following-sibling::td//i[@title = 'Heures']/input")
    WebElement effortSt_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort standard"
    @FindBy(xpath = "//td[.//span[contains(., 'Effort stand')]]/following-sibling::td//i[@title = 'Minutes']/input")
    WebElement effortSt_minutes;

    // WebElement représentant le champ "Heures" correspondant à "Effort supplémentaire"
    @FindBy(xpath = "//td[.//span[contains(., 'Effort sup')]]/following-sibling::td//i[@title = 'Heures']/input")
    WebElement effortSup_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort supplémentaire"
    @FindBy(xpath = "//td[.//span[contains(., 'Effort sup')]]/following-sibling::td//i[@title = 'Minutes']/input")
    WebElement effortSup_minutes;

    // WebElement représentant le message : "Jour du calendrier exceptionnel "test_exception" enregistré"
    @FindBy(xpath = "//span[text() = 'Jour du calendrier exceptionnel \"test_exception\" enregistré']")
    WebElement message_jourExcep;

    // WebElement représentant le champ "Nom" du calendrier correspondant à "test_exception"
    @FindBy(xpath = "//span[text() = 'test_exception']")
    WebElement cal_nom1;
    // WebElement représentant le champ "Couleur" du calendrier correspondant à "test_exception"
    @FindBy(xpath = "//td[.//span[text() = 'test_exception']]/following-sibling::td[1]//span")
    WebElement cal_couleur1;
    // WebElement représentant le champ "Sur-affecté" du calendrier correspondant à "test_exception"
    @FindBy(xpath = "//td[.//span[text() = 'test_exception']]/following-sibling::td[2]//span")
    WebElement cal_sa1;
    // WebElement représentant le champ "Effort standard" du calendrier correspondant à "test_exception"
    @FindBy(xpath = "//td[.//span[text() = 'test_exception']]/following-sibling::td[3]//span")
    WebElement cal_efStd1;
    // WebElement représentant le champ "Effort heures supplémentaires" du calendrier correspondant à "test_exception"
    @FindBy(xpath = "//td[.//span[text() = 'test_exception']]/following-sibling::td[4]//span")
    WebElement cal_efSup1;

    // WebElement représentant le bouton supprimer de : Calendrier - Test 1
    @FindBy(xpath = "//span[text()='Calendrier - Test 1']/ancestor::tr//span[@title='Supprimer']")
    WebElement iconeSup_test1;

    // WebElement représentant le bouton supprimer de : "Calendrier - Test Calendrier Dérivé"
    @FindBy(xpath = "//span[text()='Calendrier - Test Calendrier Dérivé']/ancestor::tr//span[@title='Supprimer']")
    WebElement iconeSup_derive;

    // WebElement représentant le bouton supprimer de : "test_exception"
    @FindBy(xpath = "//span[text()='test_exception']/ancestor::tr//span[@title='Supprimer']")
    WebElement iconeSup_TestExc;

    // WebElement représentant le boutton OK du message supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//*[@class='z-messagebox-btn z-button']//td[text()='OK']")
    WebElement ok_supprimer;

    // Méthode pour Ajouter une exception
    public void creerJourExceptionnel(WebDriver driver) throws InterruptedException {
        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_jourExceptionnel);

        // Assert que le titre la page "Jours exceptionnels du calendrier Liste" est affiché
        assertTrue(title_jourEceptionnel.isDisplayed());
        // Assert que la colonne "Nom" dans le tableau est affichée
        assertTrue(jourEception_nomT.isDisplayed());
        // Assert que la colonne "Couleur" dans le tableau est affichée
        assertTrue(jourEception_couleurT.isDisplayed());
        // Assert que la colonne "Sur-affecté" dans le tableau est affichée
        assertTrue(jourEception_surAffecteT.isDisplayed());
        // Assert que la colonne "Effort standard" dans le tableau est affichée
        assertTrue(jourEception_effortStandT.isDisplayed());
        // Assert que la colonne "Effort en heures supplémentaires" dans le tableau est affichée
        assertTrue(jourEception_effortSupT.isDisplayed());
        // Assert que la colonne "Opérations" dans le tableau est affichée
        assertTrue(jourEception_operationsT.isDisplayed());
        // Assert que le boutton [Créer] est affiché
        assertTrue(boutton_creer.isDisplayed());


        // Cliquer sur le boutton [Créer]
        click(boutton_creer);

        // Assert que le titre la page "Jours exceptionnels du calendrier Liste" est affiché
        assertTrue(title_creerJourExcep.isDisplayed());
        // Assert que l'onglet "Modifier" est affiché
        assertTrue(onglet_modifier.isDisplayed());

        // Assert que le camp de saisi "Code" : pas vide et non modifiable par default
        assertFalse(field_code.getAttribute("value").isEmpty());
        assertNotNull(field_code.getAttribute("disabled"));

        // Assert que la valeur par défaut est "rouge (par défaut)"
        assertEquals("rouge (par défaut)", couleur_liste.getAttribute("value"));
        // Assert que la case "Couleur" correspondante à "Exception particulière" est une nuance de rouge
        assertTrue(couleur_casePart.getAttribute("style").contains("rgb(255, 51, 51)"));
        // Assert que la case "Couleur" correspondante à "Exception héritée" est une nuance de rouge
        assertTrue(couleur_caseHeri.getAttribute("style").contains("rgb(255, 153, 153)"));

        // Assert que les boutons "Enregistrer" "Enregistrer et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());

        // Assert que le champ "Heures" correspondant à "Effort standard" = "0"
        assertEquals("0", effortSt_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort standard" = "0"
        assertEquals("0", effortSt_minutes.getAttribute("value"));
        // Assert que le champ "Heures" correspondant à "Effort supplémentaire" = "0"
        assertEquals("0", effortSup_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort supplémentaire" = "0"
        assertEquals("0", effortSup_minutes.getAttribute("value"));


        // Noter la valeur renseignée par défaut pour le champ "Code"
        String valeurCode1 = field_code.getAttribute("value");
        // Renseigner le champ "Nom" avec la valeur "test_exception"
        String jourExc1 = "test_exception";
        sendKey(field_nom, jourExc1);
        // Cliquer sur le bouton [Annuler]
        click(boutton_annuler);

        // Assert que le titre la page "Jours exceptionnels du calendrier Liste" est affiché
        assertTrue(title_jourEceptionnel.isDisplayed());
        // Assert que le jour exceptionnel n'a pas été ajouté
        assertFalse(jourException_tableau.getText().contains(jourExc1));

        // Cliquer sur le boutton [Créer]
        click(boutton_creer);

        // Assert que le titre la page "Créer Jour du calendrier exceptionnel" est affiché
        attendreElement(title_creerJourExcep);
        assertTrue(title_creerJourExcep.isDisplayed());
        // Assert que l'onglet "Modifier" est affiché
        assertTrue(onglet_modifier.isDisplayed());

        // Assert que le camp de saisi "Code" : pas vide et non modifiable par default
        assertFalse(field_code.getAttribute("value").isEmpty());
        assertNotNull(field_code.getAttribute("disabled"));

        // Assert que la valeur par défaut est "rouge (par défaut)"
        assertEquals("rouge (par défaut)", couleur_liste.getAttribute("value"));
        // Assert que la case "Couleur" correspondante à "Exception particulière" est une nuance de rouge
        assertTrue(couleur_casePart.getAttribute("style").contains("rgb(255, 51, 51)"));
        // Assert que la case "Couleur" correspondante à "Exception héritée" est une nuance de rouge
        assertTrue(couleur_caseHeri.getAttribute("style").contains("rgb(255, 153, 153)"));

        // Assert que les boutons "Enregistrer" "Enregistrer et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());

        // Assert que le champ "Heures" correspondant à "Effort standard" = "0"
        assertEquals("0", effortSt_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort standard" = "0"
        assertEquals("0", effortSt_minutes.getAttribute("value"));
        // Assert que le champ "Heures" correspondant à "Effort supplémentaire" = "0"
        assertEquals("0", effortSup_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort supplémentaire" = "0"
        assertEquals("0", effortSup_minutes.getAttribute("value"));


        // Assert que La valeur du champ "Code" = valeur avant annulation de la création + 1
        int numeroPrecedent = Integer.parseInt(valeurCode1.replaceAll("\\D", ""));
        // Noter la nouvelle valeur renseignée par défaut pour le champ "Code"
        String valeurCode2 = field_code.getAttribute("value");
        int numeroNouveau = Integer.parseInt(valeurCode2.replaceAll("\\D", ""));
        // Vérifier l'alimentation du champ "Code"
        assertEquals(numeroNouveau, numeroPrecedent + 1);

        // Renseigner le champ "Nom" avec la valeur "test_exception"
        sendKey(field_nom, jourExc1);

        // Mapping des couleurs attendues pour chaque option de couleur
        Map<String, String> couleurAttendueParticuliereMap = new HashMap<>();
        Map<String, String> couleurAttendueHeriteeMap = new HashMap<>();

        couleurAttendueParticuliereMap.put("rouge (par défaut)", "rgb(255, 51, 51)");
        couleurAttendueHeriteeMap.put("rouge (par défaut)", "rgb(255, 153, 153)");

        couleurAttendueParticuliereMap.put("vert", "rgb(46, 230, 46)");
        couleurAttendueHeriteeMap.put("vert", "rgb(138, 230, 138)");

        couleurAttendueParticuliereMap.put("bleu", "rgb(51, 51, 255)");
        couleurAttendueHeriteeMap.put("bleu", "rgb(153, 153, 255)");

        couleurAttendueParticuliereMap.put("magenta", "rgb(255, 51, 255)");
        couleurAttendueHeriteeMap.put("magenta", "rgb(255, 153, 255)");

        couleurAttendueParticuliereMap.put("cyan", "rgb(51, 255, 255)");
        couleurAttendueHeriteeMap.put("cyan", "rgb(153, 255, 255)");

        couleurAttendueParticuliereMap.put("jaune", "rgb(230, 230, 46)");
        couleurAttendueHeriteeMap.put("jaune", "rgb(230, 230, 161)");

        couleurAttendueParticuliereMap.put("noir", "rgb(51, 51, 51)");
        couleurAttendueHeriteeMap.put("noir", "rgb(153, 153, 153)");

        couleurAttendueParticuliereMap.put("orange", "rgb(255, 183, 51)");
        couleurAttendueHeriteeMap.put("orange", "rgb(255, 219, 153)");

        couleurAttendueParticuliereMap.put("violet", "rgb(128, 26, 128)");
        couleurAttendueHeriteeMap.put("violet", "rgb(179, 142, 179)");

        // Find the dropdown element
        Select dropdown = new Select(couleur_liste);

        // Itérer à travers chaque option dans la liste déroulante
        for (WebElement option : dropdown.getOptions()) {
            click(couleur_liste);
            dropdown.selectByVisibleText(option.getText());
            Thread.sleep(100);

            // Get the color displayed for "Exception particulière" and "Exception héritée"
            String couleurParticuliere = couleur_casePart.getCssValue("background-color");
            String couleurHeritee = couleur_caseHeri.getCssValue("background-color");

            // Ajoutez votre logique d'assertion ici avec une boucle switch
            switch (option.getText()) {
                case "rouge (par défaut)":
                case "vert":
                case "bleu":
                case "magenta":
                case "cyan":
                case "jaune":
                case "noir":
                case "orange":
                case "violet":
                    assert couleurParticuliere.equals(couleurAttendueParticuliereMap.get(option.getText())) :
                            "Couleur particulière incorrecte pour " + option.getText();
                    assert couleurHeritee.equals(couleurAttendueHeriteeMap.get(option.getText())) :
                            "Couleur héritée incorrecte pour " + option.getText();
                    break;

                default:
                    System.out.println("Couleurs attendues non définies pour l'option : " + option.getText());
                    break;
            }
        }

        Actions actions = new Actions(driver);
        Thread.sleep(500);
        actions.doubleClick(effortSt_heures).perform();
        effortSt_heures.sendKeys("-1");
        click(field_nom);

        // Assert que le message = "Dépassement du rang (>= 0)."
        assertEquals("Dépassement du rang (>= 0).", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        String borderStyle = cadre_error.getCssValue("border");
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());


        sendKey(effortSt_heures, "1");
        click(field_nom);
        Thread.sleep(500);
        actions.doubleClick(effortSt_minutes).perform();
        effortSt_minutes.sendKeys("-1");
        click(field_nom);

        // Assert que le message = "Dépassement du rang (>= 0)."
        assertEquals("Dépassement du rang (0 ~ 59).", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());

        sendKey(effortSt_minutes, "1");
        click(field_nom);
        Thread.sleep(500);
        actions.doubleClick(effortSup_heures).perform();
        effortSup_heures.sendKeys("-1");
        click(boutton_enregistrer);

        // Assert que le message = "Dépassement du rang (>= 0)."
        assertEquals("Dépassement du rang (>= 0).", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());


        sendKey(effortSup_heures, "5");
        click(field_nom);
        Thread.sleep(500);
        actions.doubleClick(effortSup_minutes).perform();
        effortSup_minutes.sendKeys("-1");
        click(boutton_sauver);

        // Assert que le message = "Dépassement du rang (>= 0)."
        assertEquals("Dépassement du rang (0 ~ 59).", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());

        attendreElement(effortSup_minutes);
        effortSup_minutes.clear();
        effortSup_minutes.sendKeys("3");
        click(boutton_enregistrer);

        // Assert que le titre la page "Jours exceptionnels du calendrier Liste" est affiché
        assertTrue(title_jourEceptionnel.isDisplayed());
        // Assert que le message : "Jour du calendrier exceptionnel "test_exception" enregistré" est affiché
        assertTrue(message_jourExcep.isDisplayed());

        // Assert que le champ "Nom" du calendrier correspondant à "test_exception" est ajouté
        assertTrue(cal_nom1.isDisplayed());
        // Assert le champ "Couleur" du calendrier correspondant à "test_exception"
        assertEquals("violet", cal_couleur1.getText());
        // Assert le champ "Sur-affecté" du calendrier correspondant à "test_exception"
        assertEquals("No", cal_sa1.getText());
        // Assert le champ "Effort standard" du calendrier correspondant à "test_exception"
        assertEquals("1:1", cal_efStd1.getText());
        // Assert le champ "Effort heures supplémentaires" du calendrier correspondant à "test_exception"
        assertEquals("5:3", cal_efSup1.getText());

        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_calendrier);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());
        // Assert que les colonnes "Nom" "Hérité de la date" "Héritages à jour" "Opérations" sont affichés
        assertTrue(caledrier_nom.isDisplayed());
        assertTrue(caledrier_herite.isDisplayed());
        assertTrue(caledrier_heritages.isDisplayed());
        assertTrue(caledrier_operations.isDisplayed());

        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        // Cliquer sur l'icône "Modifier" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_modifier);

        // Assert que le titre de la page "Modifier Calendrier: Calendrier - Test 1" est affiché
        attendreElement(title_modifierTest1);
        assertTrue(title_modifierTest1.isDisplayed());

        // Cliquer sur la liste déroulante associée au champ "Type d'exception"
        click(type_exception);

        // Assert que le type d'exception "test_exception"
        assertTrue(test_exception.isDisplayed());

        //Sélectionner dans le champ "Date de fin", la même date que le champ" Date de début" (soit date du jour)
        // Obtenez la date actuelle
        LocalDate dateJour = LocalDate.now();
        // Appelez la méthode pour formater la date
        String dateAujordhui = formatDate(dateJour);
        // Saisir date du jour dans le champ "Date de fin"
        //click(field_dateFin);
        sendKey(field_dateFin, dateAujordhui);

        // Cliquer sur la liste déroulante associée au champ "Type d'exception"
        click(type_exception);

        // sélectionner "test_exception" dans les valeurs
        click(test_exception);

        // Assert que le champ "Heures" correspondant à "Effort normal:" = 1
        assertEquals("1", effortN_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort normal:" = 1
        assertEquals("1", effortN_minutes.getAttribute("value"));
        // Assert que le champ "Heures" correspondant à "Effort en heures supplémentaires:" = 5
        assertEquals("5", effortS_heures.getAttribute("value"));
        // Assert que le champ "Minutes" correspondant à "Effort en heures supplémentaires:" = 3
        assertEquals("3", effortS_minutes.getAttribute("value"));

        // Cliquer sur le boutton [Créer une exception]
        click(boutton_majException);

        // Assert que le tableau "Propriétés des jours" est affichée
        assertTrue(exception_tableau.isDisplayed());
        // Assert que le "Type" : Exception: test_exception
        assertEquals("Exception: test_exception", exceptionT_type.getText());
        // Assert que le "Temps travaillé" : 1:1
        assertEquals("1:1", exceptionT_temps.getText());
        // Assert que "Jour" : la date sélectionnée dans le champ "Date de début" dans le format : d MMM yyyy
        assertEquals(dateAujordhui, exceptionT_jour.getAttribute("value"));

        // Assert que la ligne "Liste des exceptions" est affichée dans le tableau du bloc
        assertTrue(exception_liste.isDisplayed());
        // Utilisez le formateur pour obtenir la date au format "AAAA-MM-JJ"
        String exceptionDate = dateJour.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        // Assert que la Date correspondant à la date sélectionnée dans le champ "Date de début"
        assertEquals(exceptionDate, exceptionL_date2.getText());
        // Assert que le Type d'exception : type d'exception sélectionné "STRIKE"
        assertEquals("test_exception", exceptionL_type2.getText());
        // Assert que "Effort normal ": 1:1
        assertEquals("1:1", exceptionL_effortN2.getText());
        // Assert que "Effort supplément" : 5:3
        assertEquals("5:3", exceptionL_effortS2.getText());
        // Assert que le "Code" : champ vide et non modifiable
        assertTrue(exceptionL_code2.getText().isEmpty());
        assertNotNull(exceptionL_code2.getAttribute("disabled"));
        // Assert que "Origine" : Direct
        assertEquals("Direct", exceptionL_origine2.getText());
    }

    // WebElement représentant le bouton supprimer de : test_exception
    @FindBy(xpath = "//span[text()='test_exception']/ancestor::tr[1]//span[@title='Supprimer']")
    WebElement testException_sup;

    // Méthode pour Nettoyage
    public void NettoyageException (WebDriver driver) throws InterruptedException {
        // Clique sur le bouton supprimer de l'exeption
        click(testException_sup);
        // Clique sur le bouton OK
        click(boutton_enregistrer);
    }

    // Méthode pour Nettoyage
    public void NettoyageCal1 (WebDriver driver) throws InterruptedException {
        // Clique sur le bouton supprimer de derive de Test 1
        click(iconeSup_derive);
        // Clique sur le bouton OK
        click(ok_supprimer);

        // Clique sur le bouton supprimer de Tset 1
        click(iconeSup_test1);
        // Clique sur le bouton OK
        click(ok_supprimer);

        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_jourExceptionnel);
    }

    // Méthode pour Nettoyage
    public void NettoyageJourExc (WebDriver driver) throws InterruptedException {

        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_jourExceptionnel);

        // Clique sur le bouton supprimer de Tset 1
        click(iconeSup_TestExc);
        // Clique sur le bouton OK
        click(ok_supprimer);

    }
}