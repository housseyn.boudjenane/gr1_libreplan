import Outils.PageOutils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static org.junit.jupiter.api.Assertions.*;

public class PageCalendrier extends PageOutils {

    public PageCalendrier(WebDriver driver) {
        super(driver);
        myPageFactory(driver, this);
    }

    // WebElement représentant l'onglet "Ressources"
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Ressources')]")
    WebElement onglet_Ressources;

    // WebElement représentant l'item "Calendrier"
    @FindBy(xpath = "//a[@href='/libreplan/calendars/calendars.zul']")
    WebElement item_calendrier;

        // WebElement représentant le titre de la page "Liste de calendriers"
    @FindBy(xpath = "//div[text() = 'Liste de calendriers']")
    WebElement title_calendrier;

    // WebElement représentant le boutton créer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer']")
    WebElement boutton_creer;

    // WebElement représentant le titre de colonne "Nom"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Nom')]")
    WebElement caledrier_nom;

    // WebElement représentant le titre de colonne "Hérité de la date"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Hérité de la date')]")
    WebElement caledrier_herite;

    // WebElement représentant le titre de colonne "Héritages à jour"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Héritages à jour')]")
    WebElement caledrier_heritages;

    // WebElement représentant le titre de colonne "Operations"
    @FindBy(xpath = "//div[@class='z-treecol-cnt' and contains(text(), 'Opérations')]")
    WebElement caledrier_operations;

    // WebElement représentant le titre de la page "Créer calendrier"
    @FindBy(xpath = "//td[@class='z-caption-l' and text() = 'Créer Calendrier']")
    WebElement title_creerCal;

    // WebElement représentant le titre de la page "Créer Calendrier : Calendrier - Test Calendrier Dérivé"
    @FindBy(xpath = "//td[text() = 'Créer Calendrier: Calendrier - Test Calendrier Dérivé']")
    WebElement title_derviveCal;

    // WebElement représentant le titre de la page "Créer Calendrier : Calendrier - Test 1"
    @FindBy(xpath = "//td[text() = 'Créer Calendrier: Calendrier - Test 1']")
    WebElement title_copieTest1;

    // WebElement représentant le titre de la page "Modifier Calendrier : Calendrier - Test 1"
    @FindBy(xpath = "//td[text() = 'Modifier Calendrier: Calendrier - Test 1']")
    WebElement title_modifierTest1;

    // WebElement représentant l'onglet "Données de calendrier"
    @FindBy(xpath = "//span[@class='z-tab-text' and text() = 'Données de calendrier']")
    WebElement onglet_donneesCalendrier;

    // WebElement représentant le boutton "Enregistrer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer']")
    WebElement boutton_enregistrer;

    // WebElement représentant le boutton "Enregistrer et continuer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer et continuer']")
    WebElement boutton_sauver;

    // WebElement représentant le boutton "Annuler"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Annuler']")
    WebElement boutton_annuler;

    // WebElement représentant le champ Nom
    @FindBy(xpath = "//td[contains(., 'Nom')]/following-sibling::td//input")
    WebElement field_nom;

    // WebElement représentant le champ Nom
    @FindBy(xpath = "//tr[@valign='top']/td/span[@class='z-label']")
    WebElement field_type;

    // WebElement représentant le calendrier "Calendrier - Test 1" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 1']")
    WebElement calendrier_enregistre;

    // WebElement représentant le calendrier "Calendrier - Test 2" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 2']")
    WebElement calendrier_enregistre2;

    // WebElement représentant le bouton supprimer de : Calendrier - Test 2
    @FindBy(xpath = "//span[text()='Calendrier - Test 2']/ancestor::tr//span[@title='Supprimer']")
    WebElement iconeSup_test2;

    // WebElement représentant le boutton OK du message supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//*[@class='z-messagebox-btn z-button']//td[text()='OK']")
    WebElement ok_supprimer;

    // WebElement représentant l'icône "Créer une dérive" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 1']/ancestor::tr//span[@title='Créer une dérive']")
    WebElement icone_creerDerive;

    // WebElement représentant l'icône "Créer une copie" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 1']/ancestor::tr//span[@title='Créer une copie']")
    WebElement icone_copie;

    // WebElement représentant l'icône "Modifier" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Calendrier - Test 1']/ancestor::tr//span[@title='Modifier']")
    WebElement icone_modifier;

    // WebElement représentant le message : "Calendrier - Test 1 existe déjà"
    @FindBy(xpath = "//span[@class='z-label' and text() = 'Calendrier - Test 1 existe déjà']")
    WebElement message_existe;

    // WebElement représentant le cadre message : "Calendrier de base "Calendrier - Test Calendrier Dérivé" enregistré"
    @FindBy(xpath = "//span[text() = 'Calendrier - Test 1 existe déjà']/parent::div")
    WebElement warning_existe;

    // WebElement représentant le message : "Calendrier de base "Calendrier - Test Calendrier Dérivé" enregistré"
    @FindBy(xpath = "//span[text() = 'Calendrier de base \"Calendrier - Test Calendrier Dérivé\" enregistré']")
    WebElement message_derive;

    // WebElement représentant Le message : "Calendrier de base "Calendrier - Test 2" enregistré"
    @FindBy(xpath = "//span[text() = 'Calendrier de base \"Calendrier - Test 2\" enregistré']")
    WebElement message_cree2;
    // WebElement représentant le cadre du message info
    @FindBy(css = "div.message_INFO")
    WebElement cadre_info;

    // WebElement représentant Le message : "Calendrier de base "Calendrier - Test 1" enregistré"
    @FindBy(xpath = "//span[text() = 'Calendrier de base \"Calendrier - Test 1\" enregistré']")
    WebElement message_cree1;

    // WebElement représentant le sous calendrier "Calendrier - Test Calendrier Dérivé"
    @FindBy(xpath = "//span[text() = 'Calendrier - Test Calendrier Dérivé']")
    WebElement calendrier_derive;

    // WebElement représentant le bouton [-] associé au calendrier "Calendrier - Test 1"
    @FindBy(xpath = "//span[@class='z-dottree-ico z-dottree-root-open']")
    WebElement boutton_derive;

    // WebElement représentant le bouton [Créer une exception]
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer une exception']")
    WebElement boutton_exception;

    // WebElement représentant le cadre du message d'erreur
    @FindBy(css = "div.z-errbox")
    WebElement cadre_error;
    // WebElement représentant la message d'exception KO
    @FindBy(xpath = "//div[@class='z-errbox-center']")
    WebElement message_exceptionKO;
    // WebElement représentant la croix du message d'exception KO
    @FindBy(xpath = "//div[@class='z-errbox-right z-errbox-close']")
    WebElement croix_exceptionKO;
    // WebElement représentant la fleche du message d'exception KO
    @FindBy(xpath = "//div[contains(@class, 'z-errbox-left')]")
    WebElement fleche_exceptionKO;

    // WebElement représentant le menu déroulant type d'exception
    @FindBy(xpath = "//td[text() = 'NO_EXCEPTION']/ancestor::i[@class='z-combobox']/i[@class='z-combobox-btn']")
    WebElement type_exception;

    // WebElement représentant le type d'exception "STRIKE"
    @FindBy(xpath = "//td[normalize-space()='STRIKE']")
    WebElement strike_exception;

    // WebElement représentant le champ Date de fin
    @FindBy(xpath = "//td[contains(., 'Date de fin:')]/following-sibling::td//input")
    WebElement field_dateFin;

    // WebElement représentant la "Liste des exceptions"
    @FindBy(xpath = "//span[text() = 'Liste des exceptions']")
    WebElement exception_liste;

    // WebElement représentant le champ "Date de début" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[1]//span")
    WebElement exceptionL_date;

    // WebElement représentant le champ "Type d'exception" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[2]//span")
    WebElement exceptionL_type;

    // WebElement représentant le champ "Effort normal" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[3]//span")
    WebElement exceptionL_effortN;

    // WebElement représentant le champ "Effort supplément" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[4]//span")
    WebElement exceptionL_effortS;

    // WebElement représentant le champ "Code" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[5]//input")
    WebElement exceptionL_code;

    // WebElement représentant le champ "Origine" dans la "Liste des exceptions"
    @FindBy(xpath = "//fieldset//div[3]//td[6]//span")
    WebElement exceptionL_origine;

    // WebElement représentant le tableau "Propriétés des jours"
    @FindBy(xpath = "//div[text() = 'Propriétés des jours']")
    WebElement exception_tableau;

    // WebElement représentant le champ correspondant à "Jour" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//input[@class='z-datebox-inp z-datebox-right-edge z-datebox-text-disd']")
    WebElement exceptionT_jour;

    // WebElement représentant le champ correspondant à "Type" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//tr[3]//tbody[2]/tr[2]/td[2]//span")
    WebElement exceptionT_type;

    // WebElement représentant le champ correspondant à "Temps travaillé" dans le tableau "Propriétés des jours"
    @FindBy(xpath = "//tr[3]//tbody[2]/tr[3]/td[2]//span")
    WebElement exceptionT_temps;

    // WebElement représentant le champ "Heures" correspondant à "Effort normal:"
    @FindBy(xpath = "//span[text()='Effort normal:']/../following-sibling::td[2]//i[@title='Heures']/input")
    WebElement effortN_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort normal:"
    @FindBy(xpath = "//span[text()='Effort normal:']/../following-sibling::td[2]//i[@title='Minutes']/input")
    WebElement effortN_minutes;

    // WebElement représentant le champ "Heures" correspondant à "Effort en heures supplémentaires:"
    @FindBy(xpath = "//span[text()='Effort en heures supplémentaires:']/../following-sibling::td[2]//i[@title='Heures']/input")
    WebElement effortS_heures;

    // WebElement représentant le champ "Minutes" correspondant à "Effort en heures supplémentaires:"
    @FindBy(xpath = "//span[text()='Effort en heures supplémentaires:']/../following-sibling::td[2]//i[@title='Minutes']/input")
    WebElement effortS_minutes;

    // WebElement représentant le bouton [Mettre à jour l'exception]
    @FindBy(xpath = "//td[text() = \"Mettre à jour l'exception\"]")
    WebElement boutton_majException;

    // WebElement représentant le champ "Code" dans le bloc "Données de calendrier"
    @FindBy(xpath = "//td[.//span[contains(., 'Code')]]/following-sibling::td//input[contains(@class, 'z-textbox')]")
    WebElement exceptionD_code;


    // Méthode pour Créer un calendrier
    public void creerCalendrier(WebDriver driver) throws InterruptedException {
        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_calendrier);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());
        // Assert que les colonnes "Nom" "Hérité de la date" "Héritages à jour" "Opérations" sont affichés
        assertTrue(caledrier_nom.isDisplayed());
        assertTrue(caledrier_herite.isDisplayed());
        assertTrue(caledrier_heritages.isDisplayed());
        assertTrue(caledrier_operations.isDisplayed());

        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        // Cliquer sur le bouton "Créer"
        click(boutton_creer);

        // Assert que le titre de la page "Créer calendrier" est affiché
        attendreElement(title_creerCal);
        assertTrue(title_creerCal.isDisplayed());
        // Assert que l'onglet est "Données de calendrier" est affiché
        assertTrue(onglet_donneesCalendrier.isDisplayed());
        // Assert que les boutons "Enregistrer" "Enregistrer et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());

        // Saisir le Nom :" "Calendrier - Test 1" et Cliquer sur le bouton "Enregistrer"
        sendKey(field_nom, "Calendrier - Test 1");
        click(boutton_enregistrer);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        attendreElement(title_calendrier);
        assertTrue(title_calendrier.isDisplayed());

        // Assert que le calendrier "Calendrier - Test 1" est présent dans le tableau
        assertTrue(calendrier_enregistre.isDisplayed());

        // Cliquer sur l'icône "Créer une dérive" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_creerDerive);

        // Assert que le titre de la page "Créer calendrier" est affiché
        attendreElement(title_creerCal);
        assertTrue(title_creerCal.isDisplayed());
        // Assert que le champ "Nom" est vide
        assertTrue(field_nom.getText().trim().isEmpty());
        // Assert que le champ "Type" = "Dérivé du calendrier calendrier 1"
        assertEquals("Dérivé du calendrier Calendrier - Test 1", field_type.getText());

        // Saisir le Nom :" "Calendrier - Test 1" et Cliquer sur le bouton "Enregistrer et continuer"
        sendKey(field_nom, "Calendrier - Test 1");
        click(boutton_sauver);

        // Assert que le message "Calendrier - Test 1 existe déjà" est affiché
        attendreElement(message_existe);
        assertTrue(message_existe.isDisplayed());
        // et le cadre est orange
        String existeColor = warning_existe.getCssValue("background-color");
        assertEquals("#FDCA87", Color.fromString(existeColor).asHex().toUpperCase());

        // Saisir le Nom :"Calendrier - Test Calendrier Dérivé" et Cliquer sur le bouton "Enregistrer et continuer"
        sendKey(field_nom,"Calendrier - Test Calendrier Dérivé");
        click(boutton_sauver);

        // Assert que le message "Calendrier de base "Calendrier - Test Calendrier Dérivé" enregistré" est affiché
        attendreElement(message_derive);
        assertTrue(message_derive.isDisplayed());
        // Assert que le titre de la page "Créer Calendrier: Calendrier - Test Calendrier Dérivé" est affiché
        assertTrue(title_derviveCal.isDisplayed());

        // Cliquer sur le bouton "Annuler"
        click(boutton_annuler);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        attendreElement(title_calendrier);
        assertTrue(title_calendrier.isDisplayed());
        // Assert que le calendrier "Calendrier - Test 1" est présent dans le tableau
        assertTrue(calendrier_enregistre.isDisplayed());
        // Assert que le sous calendrier "Calendrier - Test Calendrier Dérivé" est présent dans le tableau
        assertTrue(calendrier_derive.isDisplayed());

        // Cliquer sur le bouton [-] associé au calendrier "Calendrier - Test 1"
        click(boutton_derive);

        // Assert que le calendrier "Calendrier - Test 1" est présent dans le tableau
        attendreElement(calendrier_enregistre);
        assertTrue(calendrier_enregistre.isDisplayed());
        // Assert que le sous calendrier "Calendrier - Test Calendrier Dérivé" est présent dans le tableau
        assertFalse(calendrier_derive.isDisplayed());

        // Cliquer sur l'icône "Créer une copie" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_copie);

        // Assert que le titre de la page est : "Créer Calendrier : Calendrier - Test 1"
        attendreElement(title_copieTest1);
        assertTrue(title_copieTest1.isDisplayed());
        // Assert que le champ  "Nom" = "Calendrier - Test 1"
        assertEquals("Calendrier - Test 1", field_nom.getAttribute("value"));
        // Assert que le champ "Type" = "Calendrier source"
        assertEquals("Calendrier source", field_type.getText());

        // Ne pas modifier le nom du calendrier puis cliquer sur le bouton [Enregistrer et continuer]
        click(boutton_sauver);

        // Assert que le message "Calendrier - Test 1 existe déjà" est affiché
        attendreElement(message_existe);
        assertTrue(message_existe.isDisplayed());
        // et le cadre est orange
        assertEquals("#FDCA87", Color.fromString(existeColor).asHex().toUpperCase());

        // Saisir le Nom :" "Calendrier - Test 2" et Cliquer sur le bouton [Enregistrer]
        sendKey(field_nom,"Calendrier - Test 2");
        click(boutton_enregistrer);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        attendreElement(title_calendrier);
        assertTrue(title_calendrier.isDisplayed());
        // Assert que Le message suivant est affiché : "Calendrier de base "Calendrier - Test 2" enregistré"
        assertTrue(message_cree2.isDisplayed());
        // Assert que le calendrier "Calendrier - Test 1" est présent dans le tableau
        assertTrue(calendrier_enregistre.isDisplayed());
        // Assert que le calendrier "Calendrier - Test 2" est présent dans le tableau
        assertTrue(calendrier_enregistre2.isDisplayed());
    }

    // Méthode pour Ajouter une exception
    public void ajouterException(WebDriver driver) throws InterruptedException {
        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Calendriers"
        moveMouse(onglet_Ressources);
        click(item_calendrier);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());
        // Assert que les colonnes "Nom" "Hérité de la date" "Héritages à jour" "Opérations" sont affichés
        assertTrue(caledrier_nom.isDisplayed());
        assertTrue(caledrier_herite.isDisplayed());
        assertTrue(caledrier_heritages.isDisplayed());
        assertTrue(caledrier_operations.isDisplayed());
        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        // Cliquer sur l'icône "Modifier" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_modifier);

        // Assert que le titre de la page "Modifier Calendrier: Calendrier - Test 1" est affiché
        attendreElement(title_modifierTest1);
        assertTrue(title_modifierTest1.isDisplayed());
        // Assert que l'onglet est "Données de calendrier" est affiché
        assertTrue(onglet_donneesCalendrier.isDisplayed());
        // Assert que les boutons "Enregistrer" "Enregistrer et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());

        // Cliquer sur le boutton [Créer une exception]
        click(boutton_exception);

        // Assert que le message = "Merci de choisir un type d'exception"
        assertEquals("Merci de choisir un type d'exception", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        String borderStyle = cadre_error.getCssValue("border");
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());

        // Cliquer sur la liste déroulante associée au champ "Type d'exception"
        click(type_exception);

        // sélectionner "STRIKE" dans les valeurs (ne pas sélectionner "HALF_DAY_HOLIDAY" et "WORKING_DAY")
        click(strike_exception);

        // Cliquer sur le boutton [Créer une exception]
        click(boutton_exception);

        // Assert que le message = "Merci de choisir une date de fin pour l'exception"
        assertEquals("Merci de choisir une date de fin pour l'exception", message_exceptionKO.getText());
        // Assert que la bordure du cadre d'exception est rouge
        assertEquals("1px solid rgb(255, 0, 0)", borderStyle);;
        // Assert que le cadre rouge contient une flèche dirigée vers le champ "Date de fin" et une croix :
        assertTrue(fleche_exceptionKO.isDisplayed());
        assertTrue(croix_exceptionKO.isDisplayed());

        //Sélectionner dans le champ "Date de fin", la même date que le champ" Date de début" (soit date du jour)
        // Obtenez la date actuelle
        LocalDate dateJour = LocalDate.now();
        // Appelez la méthode pour formater la date
        String dateAujordhui = formatDate(dateJour);
        // Saisir date du jour dans le champ "Date de fin"
        //click(field_dateFin);
        sendKey(field_dateFin,dateAujordhui);

        // Cliquer sur le boutton [Créer une exception]
        click(boutton_exception);

        // Assert que la ligne "Liste des exceptions" est affichée dans le tableau du bloc
        assertTrue(exception_liste.isDisplayed());
        // Utilisez le formateur pour obtenir la date au format "AAAA-MM-JJ"
        String exceptionDate = dateJour.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        // Assert que la Date correspondant à la date sélectionnée dans le champ "Date de début"
        assertEquals(exceptionDate,exceptionL_date.getText());
        // Assert que le Type d'exception : type d'exception sélectionné "STRIKE"
        assertEquals("STRIKE",exceptionL_type.getText());
        // Assert que "Effort normal ": 0:0
        assertEquals("0:0",exceptionL_effortN.getText());
        // Assert que "Effort supplément" : 0:0
        assertEquals("0:0",exceptionL_effortS.getText());
        // Assert que le "Code" : champ vide et non modifiable
        assertTrue(exceptionL_code.getText().isEmpty());
        assertNotNull(exceptionL_code.getAttribute("disabled"));
        // Assert que "Origine" : Direct
        assertEquals("Direct",exceptionL_origine.getText());

        // Assert que le tableau "Propriétés des jours" est affichée
        assertTrue(exception_tableau.isDisplayed());
        // Assert que le "Type" : Exception: Strike
        assertEquals("Exception: STRIKE",exceptionT_type.getText());
        // Assert que le "Temps travaillé" : 0:0
        assertEquals("0:0",exceptionT_temps.getText());
        // Assert que "Jour" : la date sélectionnée dans le champ "Date de début" dans le format : d MMM yyyy
        assertEquals(dateAujordhui,exceptionT_jour.getAttribute("value"));

        // Saisir dans le champ "Heures" correspondant à "Effort normal:"
        effortN_heures.sendKeys("1");
        // Saisir dans le champ "Minutes" correspondant à "Effort normal:"
        effortN_minutes.sendKeys("2");
        // Saisir dans le champ "Heures" correspondant à "Effort en heures supplémentaires:"
        effortS_heures.sendKeys("8");
        // Saisir dans le champ "Minutes" correspondant à "Effort en heures supplémentaires:"
        effortS_minutes.click();
        effortS_minutes.sendKeys("4");
        // Cliquer sur le bouton [Mettre à jour l'exception]
        Thread.sleep(500);
        click(boutton_majException);

        // Assert que la ligne "Liste des exceptions" est affichée dans le tableau du bloc
        assertTrue(exception_liste.isDisplayed());
        // Assert que "Effort normal ": 1:2
        assertEquals("1:2",exceptionL_effortN.getText());
        // Assert que "Effort supplément" : 3:4
        assertEquals("8:4",exceptionL_effortS.getText());

        // Assert que le tableau "Propriétés des jours" est affichée
        assertTrue(exception_tableau.isDisplayed());
        // Assert que le "Temps travaillé" : 1:2
        assertEquals("1:2",exceptionT_temps.getText());

        // Cliquer sur le bouton [Enregistrer]
        click(boutton_enregistrer);

        // Assert que Le message suivant est affiché : "Calendrier de base "Calendrier - Test 1" enregistré"
        attendreElement(message_cree1);
        assertTrue(message_cree1.isDisplayed());
        // Assert que la bordure du cadre d'exception est vert
        String borderInfo = cadre_info.getCssValue("border");
        assertEquals("1px solid rgb(0, 102, 0)", borderInfo);
        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());

        // Cliquer sur l'icône "Modifier" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_modifier);

        // Assert que le titre de la page "Modifier Calendrier: Calendrier - Test 1" est affiché
        assertTrue(title_modifierTest1.isDisplayed());
        // Assert que l'onglet est "Données de calendrier" est affiché
        assertTrue(onglet_donneesCalendrier.isDisplayed());
        // Assert que les boutons "Enregistrer" "Enregistrer et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());

        // Assert que le "Code" : non modifiable
        assertNotNull(exceptionL_code.getAttribute("disabled"));
        //  Assert que valeur du "Code" dans "Données de calendrier" = "Code" dans "Liste des exceptions" - "-000Y"
        String exceL_code = exceptionL_code.getAttribute("value");
        String exceD_code = exceptionD_code.getAttribute("value");
        assertEquals(exceD_code,exceL_code.substring(0, exceL_code.length() - 5));

        // Cliquer sur le bouton [Annuler]
        click(boutton_annuler);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());

        // Cliquer sur l'icône "Créer une dérive" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_creerDerive);

        // Assert que le titre de la page "Créer calendrier" est affiché
        attendreElement(title_creerCal);
        assertTrue(title_creerCal.isDisplayed());
        // Assert que le champ "Nom" est vide
        assertTrue(field_nom.getText().trim().isEmpty());
        // Assert que le champ "Type" = "Dérivé du calendrier calendrier 1"
        assertEquals("Dérivé du calendrier Calendrier - Test 1", field_type.getText());

        // Assert que la ligne "Liste des exceptions" est affichée dans le tableau du bloc
        assertTrue(exception_liste.isDisplayed());
        // Assert que "Effort normal ": 1:2
        assertEquals("1:2",exceptionL_effortN.getText());
        // Assert que "Effort supplément" : 3:4
        assertEquals("8:4",exceptionL_effortS.getText());

        // Assert que le tableau "Propriétés des jours" est affichée
        assertTrue(exception_tableau.isDisplayed());
        // Assert que le "Temps travaillé" : 1:2
        assertEquals("1:2",exceptionT_temps.getText());

        // Cliquer sur le bouton [Annuler]
        click(boutton_annuler);

        // Assert que le titre de la page "Liste de calendriers" est affiché
        assertTrue(title_calendrier.isDisplayed());

        // Cliquer sur l'icône "Créer une copie" dans la colonne "Opération" pour le calendrier "Calendrier - Test 1"
        click(icone_copie);

        // Assert que le titre de la page est : "Créer Calendrier : Calendrier - Test 1"
        attendreElement(title_copieTest1);
        assertTrue(title_copieTest1.isDisplayed());
        // Assert que le champ  "Nom" = "Calendrier - Test 1"
        assertEquals("Calendrier - Test 1", field_nom.getAttribute("value"));
        // Assert que le champ "Type" = "Calendrier source"
        assertEquals("Calendrier source", field_type.getText());

        // Assert que la ligne "Liste des exceptions" est affichée dans le tableau du bloc
        assertTrue(exception_liste.isDisplayed());
        // Assert que "Effort normal ": 1:2
        assertEquals("1:2",exceptionL_effortN.getText());
        // Assert que "Effort supplément" : 3:4
        assertEquals("8:4",exceptionL_effortS.getText());

        // Assert que le tableau "Propriétés des jours" est affiché
        assertTrue(exception_tableau.isDisplayed());
        // Assert que le "Temps travaillé" : 1:2
        assertEquals("1:2",exceptionT_temps.getText());
    }

    // Méthode pour Nettoyage
    public void NettoyageCal2 (WebDriver driver) throws InterruptedException {

        // Bouger la souri sur l'onglet ressources
        moveMouse(onglet_Ressources);
        // Cliquer sur l'item criteres
        click(item_calendrier);
        // Clique sur le bouton supprimmer de Test 2
        click(iconeSup_test2);
        // Clique sur le bouton OK
        click(ok_supprimer);
    }

    // WebElement représentant le bouton supprimer de : Calendrier - Test 2
    @FindBy(xpath = "//span[text()='STRIKE']/ancestor::tr[1]//span[@title='Supprimer']")
    WebElement testException_sup;

//    public void NettoyageException (WebDriver driver) throws InterruptedException {
//        // Clique sur le bouton supprimmer de l'exception
//        click(testException_sup);
//        // Clique sur le bouton OK
//        click(boutton_enregistrer);
//    }

}