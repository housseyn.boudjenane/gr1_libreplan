import Outils.PageOutils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class PageProjet extends PageOutils {

    public PageProjet(WebDriver driver) {
        super(driver);
        myPageFactory(driver, this);
    }

    // Obtenir la date actuelle
    Date dateActuelle = new Date();

    // Déclarer variables
    private String nom1;
//    private Date datePlus5Jours;
//    private Date datePlus15Jours;
//    private int moisDebut;
//    private int anneeDebut;
//    private int moisFin;
//    private int anneeFin;


    //////////

    // WebElement représentant le logo
    @FindBy(xpath = "//td[@class='logo-area']")
    WebElement logo;

    // WebElement représentant l'onglet calendrier
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Calendrier')]")
    WebElement onglet_calendrier;
    // WebElement représentant l'item "Projets"
    @FindBy(xpath = "//a[@href='/libreplan/planner/index.zul;orders_list']")
    WebElement calendrier_item_projets;

    //////////
    // Icône "Créer un nouveau projet"
    // WebElement représentant l'icône "Créer un nouveau projet" (1ère icône "+" située en dessous du logo "LibrePlan").
    @FindBy(xpath = "//img[@src='/libreplan/common/img/ico_add.png']")
    WebElement icone_creer_nouveau_projet;

    // WebElement représentant l'élément "Nom" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[1]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_nom;

    // WebElement représentant le champ "Nom" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[1]/td[2]/div[1]/input[1]")
    WebElement nouveau_projet_champ_nom;

    // WebElement représentant l'élément "Modèle" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[2]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_modele;

    // WebElement représentant le champ "Modèle" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[2]/td[2]/div[1]/span[1]/i[1]/input[1]")
    WebElement nouveau_projet_champ_modele;

    // WebElement représentant la loupe "Modèle" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[2]/td[2]/div[1]/span[1]/i[1]/i[1]")
    WebElement nouveau_projet_loupe_modele;

    // WebElement représentant la liste "Modèle" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-bandbox-pp z-bandbox-shadow']")
    WebElement nouveau_projet_liste_modele;

    // WebElement représentant l'élément "Code" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[3]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_code;

    // WebElement représentant le champ "Code" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[3]/td[2]/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/input[1]")
    WebElement nouveau_projet_champ_code;

    // WebElement représentant l'élément "Générer le code" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//table[@class='z-hbox']/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[3]/span[1]/label[1]")
    WebElement nouveau_projet_generer_code;

    // WebElement représentant le checkbox "Générer le code" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//table[@class='z-hbox']/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[3]/span[1]/input[1]")
    WebElement nouveau_projet_checkbox_generer_code;

    // WebElement représentant l'élément "Date de début" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[4]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_date_debut;

    // WebElement représentant le champ "Date de début" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[4]/td[2]/div[1]/i[1]/input[1]")
    WebElement nouveau_projet_champ_date_debut;

    // WebElement représentant l'élément "Echéance" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[5]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_echeance;

    // WebElement représentant l'élément "Echéance" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[5]/td[2]/div[1]/i[1]/input[1]")
    WebElement nouveau_projet_champ_echeance;

    // WebElement représentant l'élément "Client" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[6]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_client;

    // WebElement représentant l'élément "Client" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[6]/td[2]/div[1]/span[1]/i[1]/input[1]")
    WebElement nouveau_projet_champ_client;

    // WebElement représentant la loupe "Client" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[6]/td[2]/div[1]/span[1]/i[1]/i[1]")
    WebElement nouveau_projet_loupe_client;

    // WebElement représentant l'élément "Client" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-bandbox-pp z-bandbox-shadow']")
    WebElement nouveau_projet_liste_client;

    // WebElement représentant l'élément "Calendrier" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[7]/td[1]/div[1]/span[1]")
    WebElement nouveau_projet_calendrier;

    // WebElement représentant le champ "Calendrier" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[7]/td[2]/div[1]/i[1]/input")
    WebElement nouveau_projet_champ_calendrier;

    // WebElement représentant la loupe "Calendrier" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//div[@class='z-grid-body']/table[1]/tbody[2]/tr[7]/td[2]/div[1]/i[1]/i[1]")
    WebElement nouveau_projet_loupe_calendrier;

    // WebElement représentant la liste "Calendrier" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "/html[1]/body[1]/div[6]/table[1]/tbody[1]/tr[1]/td[2]")
    WebElement nouveau_projet_liste_calendrier;

    // WebElement représentant le bouton "Accepter" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//span[@class='save-button global-action z-button']/table[1]/tbody[1]/tr[2]/td[2]")
    WebElement nouveau_projet_bouton_accepter;

    // WebElement représentant le bouton "Annuler" dans pop_up "Créer un nouveau projet"
    @FindBy(xpath = "//span[@class='cancel-button global-action z-button']/table[1]/tbody[1]/tr[2]/td[2]")
    WebElement nouveau_projet_bouton_annuler;

    //////////
    // Menu vertical à gauche de la page
    // WebElement représentant le menu vertical à gauche de la page
    @FindBy(xpath = "//td[@align='left']/span[1]/table[1]/tbody[1]/tr[2]/td[2]")
    List<WebElement> projet_menu_vertical;

    // WebElement représentant l'onglet' "Détail du projet" dans le menu vertical à gauche de la page
//    @FindBy(xpath = "//table[@class='z-vbox']/tbody[1]/tr[1]/td[1]/" +
//            "table[1]/tbody[1]/tr[3]/td[1]/" +
//            "table[1]/tbody[1]/tr[1]/td[1]/" +
//            "table[1]/tbody[1]/tr[5]/td[1]/span[1]/table[1]/tbody[1]/tr[2]/td[2]")
//    WebElement projet_onglet_detail_projet;

    // WebElement représentant l'onglet "Planification de projet" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//td[contains(text(),'Planification des projets')]")
    WebElement projet_onglet_planification_projets;

    //    // WebElement représentant l'onglet "Planification de projet" dans le menu vertical à gauche de la page
//    @FindBy(xpath = "//td[contains(text(),'Planification de projet')]")
//    WebElement projet_onglet_planification_projet;

    // WebElement représentant la liste déroulante "Zoom" dans l'onglet "Planification de projet"
    @FindBy(xpath = "//div[@class='toolbar-box z-north z-north-noborder']/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[15]/select[1]")
    WebElement projet_zoom;

    // WebElements représentant les options dans la liste déroulante "Zoom" dans l'onglet "Planification de projet"
    @FindBy(xpath = "//div[@class='toolbar-box z-north z-north-noborder']/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[15]/select[1]/option")
    List<WebElement> projet_zoom_options;

    // WebElements représentant l'affichage des années selon "Zoom" dans l'onglet "Planification de projet"
    @FindBy(xpath = "//tr[@class='z-columns']/td")
    List<WebElement> projet_affichage_annees;

    // WebElement représentant les barres verticales (noir) pour représenter les échéances dans l'onglet "Planification de projet"
    @FindBy(css = "div.deadline")
    List<WebElement> projet_planification_barres_verticales;

    // WebElement représentant les barres horizontales (bleu clair) pour représenter Date début à Date fin dans l'onglet "Planification de projet"
    @FindBy(css = "div.box standard-task yui-resize unassigned, div.box standard-task unassigned yui-resize")
    List<WebElement> projet_planification_barres_horizontales1;

    // WebElement représentant les barres horizontales (bleu clair) pour représenter Date début à Date fin dans l'onglet "Planification de projet"
    @FindBy(css = "div.box standard-task assigned")
    List<WebElement> projet_planification_barres_horizontales2;

    // WebElement représentant la barre horizontale bleue/1ere ligne dans l'onglet "Planification de projet"
    @FindBy(xpath = "//div[@id='listtasks']/div[1]/div[1]")
    WebElement projet_tache1_barre;

    // WebElements représentant la liste d'actions possibles sur une tache/1ere ligne dans l'onglet "Planification de projet"
    @FindBy(xpath = "/html[1]/body[1]/div[4]/ul[1]/li/div[1]/div[1]/div[1]/a[1]")
    List<WebElement> projet_tache1_liste_actions;

    // WebElement représentant "Allocation de ressources" dans la liste d'actions possibles sur une tache/1ere ligne dans l'onglet "Planification de projet"
    @FindBy(xpath = "/html[1]/body[1]/div[4]/ul[1]/li/div[1]/div[1]/div[1]/a[contains(text(),' Allocation de ressources')]")
    WebElement projet_tache1_action_allocation_ressources;

    // WebElement représentant la pop-up "Modifier la tâche : XXX" (XXX étant le nom de la tâche)/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[@class='z-window-modal-header z-window-modal-header-move']")
//            WebElement projet_tache1_action_allocation_ressources_popup;
            List<WebElement> projet_tache1_action_allocation_ressources_popup;

    // WebElement représentant l'onglet "Propriétés de la tâche"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//span[contains(text(),'Propriétés de la tâche')]")
    WebElement projet_tache1_action_allocation_ressources_onglet1;

    // WebElement représentant l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//span[contains(text(),'Allocation de ressource normale')]")
    WebElement projet_tache1_action_allocation_ressources_onglet2;

    // WebElement représentant le champ de saisie "Choisir les critères ou les ressources" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//table[@class='add-resources-or-criteria z-hbox']/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[1]/i[1]/input[1]")
    WebElement projet_tache1_action_allocation_onglet2_ressources_champ_ressource;

    // WebElement représentant la loupe associée au champ de saisie "Choisir les critères ou les ressources" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//table[@class='add-resources-or-criteria z-hbox']/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[1]/i[1]/i[1]")
    WebElement projet_tache1_action_allocation_ressources_onglet2_loupe;

    // WebElement représentant la liste déroulante contenant toutes les ressources dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[@class='z-bandbox-pp z-bandbox-shadow']/div[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[3]/" +
            "table[1]/tbody[2]/tr/td/div[1]")
    List<WebElement> projet_tache1_action_allocation_ressources_onglet2_liste;

    // WebElements représentant les ressources à sélectionner dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[contains(text(),'Resource ( Worker )')]/parent::td/preceding-sibling::td/div[1]")
//                    "//div[@class='z-bandbox-pp z-bandbox-shadow']/div[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
//                    "table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[3]/" +
//                    "table[1]/tbody[2]/tr/td/div[contains(text(),'Resource ( Worker )')]/parent::td/preceding-sibling::td/div[1]")
            List<WebElement> projet_tache1_action_allocation_ressources_onglet2_ressources;

    // WebElement représentant le bouton "Ajouter" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//table[@class='add-resources-or-criteria z-hbox']/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[3]")
    WebElement projet_tache1_action_allocation_ressources_onglet2_bouton_ajouter;

    // WebElements représentant le tableau "Allocations" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[@class='z-window-modal-cnt-noborder']/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/" +
            "fieldset[1]/div[1]/div[3]/div[3]/table[1]/tbody[2]/tr/td[2]/div[1]/span[1]")
    List<WebElement> projet_tache1_action_allocation_ressources_onglet2_tableau2;

    // WebElement représentant le bouton "Accepter" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[@class='z-window-modal-cnt-noborder']/table[2]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[@class='save-button global-action z-button']/" +
            "table[1]/tbody[1]/tr[2]/td[2]")
    WebElement projet_tache1_action_allocation_ressources_onglet2_bouton_accepter;

    // WebElement représentant le bouton "Annuler" dans l'onglet "Allocation de ressource normale"/1ere ligne (Tache2-P1)
    @FindBy(xpath = "//div[@class='z-window-modal-cnt-noborder']/table[2]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[@class='cancel-button global-action z-button']/" +
            "table[1]/tbody[1]/tr[2]/td[2]")
    WebElement projet_tache1_action_allocation_ressources_onglet2_bouton_annuler;


    // WebElement représentant l'onglet "Liste des projets" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//td[@class='z-button-cm' and contains(text(),'Liste des projets')]")
    WebElement projet_onglet_liste_projets;

    // WebElement représentant l'onglet "Chargement des resssources" dans le menu vertical à gauche de la page
    // WebElements représentant la liste des ressources dans l'onglet "Chargement des resssources" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//div[@class='resourceloadleftpane z-tree']/div[1]/table[1]/tbody[1]/tr/td[1]/div[1]/div[1]/span[1]|" +
            "//div[@class='resourceloadleftpane z-tree']/div[1]/table[1]/tbody[1]/tr/td[1]/div[1]/div[1]/span[1]/parent::div/preceding-sibling::span[1]")
    ////div[@class='resourceloadleftpane z-tree']/div[1]/table[1]/tbody[1]/tr/td[1]/div[1]/div[1]/span[1]
            List<WebElement> projet_noms_ressources;

//        // WebElement représentant l'icône affichée avant le nom de la ressource/1ere ligne dans l'onglet "Chargement des resssources" dans le menu vertical à gauche de la page
//        @FindBy(xpath = "//div[@class='resourceloadleftpane z-tree']/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/span[1]")
//        List<WebElement> projet_icone_nom1;

//        // WebElement représentant l'arborescence Nom de la tache/1 ere ligne dans l'onglet "Chargement des resssources" dans le menu vertical à gauche de la page
//        @FindBy(xpath = "//div[@class='resourceloadleftpane z-tree']/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[1]/span[1]/parent::div/preceding-sibling::span[1]")
//        WebElement projet_arbo_tache1;

    // WebElements représentant les barres horizontales (jaune) pour représenter la charge de la ressource dans l'onglet "Planification de projet"
    @FindBy(css = "div.taskassignmentinterval FULL_LOAD")
    List<WebElement> projet_ressources_barres_horizontales;

    // WebElement représentant l'infobulle du bouton "Enregistrer le projet" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//div[@class='z-west-body']/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[1]")
    WebElement projet_infobulle_enregistrer;

    // WebElement représentant l'icone "Enregistrer le projet" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//div[@class='z-west-body']/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[1]/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_enregistrer;

    // WebElement représentant le message "Projet enregistré" de la pop-up "Information"
    @FindBy(xpath = "//span[contains(text(),'Projet enregistré')]")
    WebElement projet_enregistrer_message;

    // WebElement représentant le bouton "OK" de la pop-up "Information"
    @FindBy(xpath = "//td[contains(text(),'OK')]")
    WebElement projet_enregistrer_bouton_OK;

    // WebElement représentant le bouton "x" de la pop-up "Information"
    @FindBy(xpath = "//div[@class='z-window-modal-icon z-window-modal-close']")
    WebElement projet_enregistrer_bouton_x;

    // WebElement représentant l'infobulle du bouton "Annuler l'édition" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//div[@class='z-west-body']/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[1]")
    WebElement projet_infobulle_annuler;

    // WebElement représentant l'icone "Annuler l'édition" dans le menu vertical à gauche de la page
    @FindBy(xpath = "//div[@class='z-west-body']/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[1]/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_annuler;

    // WebElement représentant la pop-up "Confirmer la fenêtre de sortie"
    @FindBy(xpath = "//div[@class='z-window-modal-header z-window-modal-header-move' and " +
            "contains(text(),'Confirmer la fenêtre de sortie')]")
    List<WebElement> projet_popup_annuler;

    // WebElement représentant le contenu de la pop-up "Confirmer la fenêtre de sortie"
    @FindBy(xpath = "//div[@class='z-messagebox']/" +
            "span[contains(text(),'Les modifications non enregistrées seront perdues. Êtes-vous sûr ?')]")
    WebElement projet_popup_annuler_contenu;

    // WebElement représentant le bouton "OK" de la pop-up "Confirmer la fenêtre de sortie"
    @FindBy(xpath = "//span[@class='z-messagebox-btn z-button']/table[1]/tbody[1]/tr[2]/td[contains(text(),'OK')]")
    WebElement projet_popup_annuler_ok;

    // WebElement représentant le bouton "Annuler" de la pop-up "Confirmer la fenêtre de sortie"
    @FindBy(xpath = "//span[@class='z-messagebox-btn z-button']/table[1]/tbody[1]/tr[2]/td[contains(text(),'Annuler')]")
    WebElement projet_popup_annuler_annuler;

    //////////
    // Menu horizontal de la page
    // WebElement représentant le menu horizontal de la page
    @FindBy(xpath = "//div[@class='z-center-body']/span[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/" +
            "div[1]/div[1]/div[1]/div[3]/ul[1]/li/div[1]/div[1]/div[1]/span[1]")
    List<WebElement> projet_menu_horizontal;

    // WebElement représentant l'onglet "WBS (tâches)" dans le menu horizontal de la page
    @FindBy(xpath = "//div[@class='main-area z-center']/div[1]/span[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/" +
            "div[1]/div[1]/div[1]/div[3]/ul[1]/li[1]/div[1]/div[1]/div[1]/span[1]")
    WebElement projet_onglet_wbs;
    // WebElement représentant le champ "Nouvelle tâche" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/span[1]/" +
            "div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/span[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[5]/input[1]")
    ////tr[@valign='middle']/td[1]/table[1]/tbody[1]/tr[1]/td[5]/input[@class='z-textbox']
            WebElement projet_wbs_nouvelle_tache;

    // WebElement représentant le champ "Heures" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tr[@valign='middle']/td[1]/table[1]/tbody[1]/tr[1]/td[9]/input[@class='z-intbox']")
    WebElement projet_wbs_heures;

    // WebElement représentant le bouton "Ajouter" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tr[@valign='middle']/td[1]/table[1]/tbody[1]/tr[1]/td[11]/span[1]/" +
            "table[1]/tbody[1]/tr[2]/td[contains(text(),'Ajouter')]")
    WebElement projet_wbs_bouton_ajouter;

    // WebElement représentant le bouton "Descendre la tâche sélectionnée" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//span[@title='Descendre la tâche sélectionnée']")
    WebElement projet_wbs_descendre_tache;

    // WebElement représentant le bouton "Remonter la tâche sélectionnée" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//span[@title='Remonter la tâche sélectionnée']")
    WebElement projet_wbs_monte_tache;

    // WebElements représentant les en-tetes du tableau dans l'onglet "WBS (tâches)"
    // "Etat de la prévision", "Code", "Nom", "Heures", "Budget", "Doit débuter après", "Echéance", "Op."
    @FindBy(xpath = "//div[@class='orderTree z-dottree']/div[1]/table[1]/tbody[2]/tr[1]/th/div[@class='z-treecol-cnt']")
    List<WebElement> projet_wbs_tableau_entetes;

    // WebElement représentant l'infobulle "Tache1-P1. Avancement:0." du tableau dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]")
    WebElement projet_wbs_infobulle_tache1;

//        // WebElement représentant l'icone "Tache1-P1. Avancement:0." dans l'onglet "WBS (tâches)"
//        @FindBy(xpath = "")
//        WebElement projet_wbs_icone_tache1; // Absence l'icone sur la page et le DOM

    // WebElement représentant l'infobulle "Déprogrammé" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td[1]/div[1]/span[2]/div[1]/span[3]")
    WebElement projet_wbs_infobulle_deprogramme1;

    // WebElement représentant l'icone "Déprogrammé" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td[1]/div[1]/span[2]/div[1]/span[3]/table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_wbs_icone_deprogramme1; // src="/libreplan/common/img/ico_unschedule1.png"

    // WebElements représentant les cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
    // "Code", "Nom", "Heures", "Budget", "Doit débuter après", "Echéance"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td/div/input|" +
            "//tbody[@class='z-treechildren']/tr[1]/td/div/table/tbody/tr/td/table/tbody/tr/td/input")
    List<WebElement> projet_wbs_tableau_cellules1;

    // WebElements représentant les cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
    // "Code", "Nom", "Heures", "Budget", "Doit débuter après", "Echéance"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[2]/td/div/input|" +
            "//tbody[@class='z-treechildren']/tr[2]/td/div/table/tbody/tr/td/table/tbody/tr/td/input")
    List<WebElement> projet_wbs_tableau_cellules2;

    // WebElements représentant les cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
    // "Code", "Nom", "Heures", "Budget", "Doit débuter après", "Echéance"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[3]/td/div/input|" +
            "//tbody[@class='z-treechildren']/tr[3]/td/div/table/tbody/tr/td/table/tbody/tr/td/input")
    List<WebElement> projet_wbs_tableau_cellules3;

    // WebElements représentant les cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
    // "Code", "Nom", "Heures", "Budget", "Doit débuter après", "Echéance"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[4]/td/div/input|" +
            "//tbody[@class='z-treechildren']/tr[4]/td/div/table/tbody/tr/td/table/tbody/tr/td/input")
    List<WebElement> projet_wbs_tableau_cellules4;

    // WebElements représentant la liste des taches dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr/td[3]/div[1]/input")
    // //tbody[@class='z-treechildren']/tr
            List<WebElement> projet_wbs_tableau_taches;

    // WebElement représentant la tache/1ere ligne dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td[3]/div[1]/input")
    WebElement projet_wbs_tache1;

    // WebElement représentant la tache/3e ligne dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[3]/td[3]/div[1]/input")
    WebElement projet_wbs_tache3;

    // WebElement représentant l'icone "Modifier" (1ere ligne) dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td[8]/div[1]/span[@title='Modifier']/table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_wbs_icone_modifier; // src="/libreplan/common/img/ico_editar1.png"

    // WebElement représentant l'icone "Supprimer" dans l'onglet "WBS (tâches)"
    @FindBy(xpath = "//tbody[@class='z-treechildren']/tr[1]/td[8]/div[1]/span[@title='Supprimer']/table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_wbs_icone_supprimer; // src="/libreplan/common/img/ico_borrar1.png"

    //////////
    // Contenu de la liste des projets
    // WebElement représentant la liste des projets
    @FindBy(xpath = "//div[@class='clickable-rows projects-list z-grid']")
    WebElement projet_liste_projets;

    // WebElement représentant l'en tete "Nom" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Nom')]")
    WebElement projet_liste_projets_nom;
    // WebElement représentant Nom du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[1]/div[1]/span[1]")
    WebElement projet_nom1;

    // WebElement représentant l'en tete "Code" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Code')]")
    WebElement projet_liste_projets_code;
    // WebElement représentant Code du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[2]/div[1]/span[1]")
    WebElement projet_code1;

    // WebElement représentant l'en tete "Date de début" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Date de début')]")
    WebElement projet_liste_projets_debut;
    // WebElement représentant Date de début du projet créé
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[3]/div[1]/span[1]")
    WebElement projet_debut1;

    // WebElement représentant l'en tete "Echeance" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Echéance')]")
    WebElement projet_liste_projets_echeance;
    // WebElement représentant Echeance du projet créé
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[4]/div[1]/span[1]")
    WebElement projet_echeance1;

    // WebElement représentant l'en tete "Client" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Client')]")
    WebElement projet_liste_projets_client;
    // WebElement représentant Client du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[5]/div[1]/span[1]")
    WebElement projet_client1;

    // WebElement représentant l'en tete "Budget total" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Budget total')]")
    WebElement projet_liste_projets_budget;
    // WebElement représentant Client du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[6]/div[1]/span[1]")
    WebElement projet_budget1;

    // WebElement représentant l'en tete "Heures" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Heures')]")
    WebElement projet_liste_projets_heures;
    // WebElement représentant Heures du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[7]/div[1]/span[1]")
    WebElement projet_heures1;

    // WebElement représentant l'en tete "Etat" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Etat')]")
    WebElement projet_liste_projets_etat;
    // WebElement représentant Etat du projet créé (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[8]/div[1]/span[1]")
    WebElement projet_etat1;

    // WebElement représentant l'en tete "Opérations" de la liste
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains (text(), 'Opérations')]")
    WebElement projet_liste_projets_operations;
    // WebElement représentant l'icone "Modifier" (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[9]/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/span[@title='Modifier']/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_modifier1;

    // WebElement représentant l'icone "Supprimer" (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[9]/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[3]/span[@title='Supprimer']/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_supprimer1;
        // WebElement représentant le bouton "OK" dans la pop-up de confirmation de suprression de projet
        @FindBy(xpath = "//td[@class='z-button-cm' and text()='OK']")
        WebElement projet_supprimer1_ok;

    // WebElement représentant l'icone "Voir la prévision" (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[9]/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[5]/span[@title='Voir la prévision']/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_prevision1;

    // WebElement représentant l'icone "Créer un modèle" (1ere ligne)
    @FindBy(xpath = "//tr[@class='clickable-rows projects-list z-row']/td[9]/div[1]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td[7]/span[@title='Créer un modèle']/" +
            "table[1]/tbody[1]/tr[2]/td[2]/img")
    WebElement projet_icone_modele1;

    //////////
    // Détail du projet / Fil d'ariane
    // WebElements représentant le fil d'ariane
    @FindBy(xpath = "//td[@class='migas_linea']/table[1]/tbody[1]/tr[1]/td[2]/strong|" +
            "//td[@class='migas_linea']/table[1]/tbody[1]/tr[1]/td[4]/" +
            "table[1]/tbody[1]/tr[1]/td[1]/" +
            "table[1]/tbody[1]/tr[1]/td/span[@class='z-label']")
    List<WebElement> fil_ariane;
    ;


    //////////
    // Méthode d'accès au formulaire de création d'un projet et de vérification de la pop-up "Créer un nouveau projet"
    public PageProjet clicCreerProjet(WebDriver driver) throws InterruptedException {
        // Cliquer sur l'icône "Créer un nouveau projet"
        click(icone_creer_nouveau_projet);

        // Attendre la pop-up affichée
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(nouveau_projet_nom));

        // Vérifier les éléments de la pop-up
        //Nom : Champ de saisie non renseigné
        assertEquals("Nom", nouveau_projet_nom.getText());
        assertTrue(nouveau_projet_champ_nom.getAttribute("value").isEmpty());

        //Modèle : Liste déroulante non renseignée
        assertEquals("Modèle", nouveau_projet_modele.getText());
        assertTrue(nouveau_projet_champ_nom.getAttribute("value").isEmpty());
        click(nouveau_projet_loupe_modele);
        assertTrue(nouveau_projet_liste_modele.isDisplayed());
        click(nouveau_projet_loupe_modele);

        //Code : Champ de saisie renseigné avec une valeur par défaut non modifiable
        assertEquals("Code", nouveau_projet_code.getText());
        assertFalse(nouveau_projet_champ_code.getAttribute("value").isEmpty());
        assertFalse(nouveau_projet_champ_code.getAttribute("disabled").isEmpty());

        //Case "Générer le code" cochée par défaut
        assertEquals("Générer le code", nouveau_projet_generer_code.getText());
        assertTrue(nouveau_projet_checkbox_generer_code.getAttribute("type").equalsIgnoreCase("checkbox"));
        assertTrue(nouveau_projet_checkbox_generer_code.isEnabled());
                /*Date de début : champ de saisie de date associé à un calendrier.
                Par défaut, la date affichée est la date du jour.
                Le format de la date correspond à l'exemple suivant : "25 janv. 2017"
                */
        assertEquals("Date de début", nouveau_projet_date_debut.getText());
        assertFalse(nouveau_projet_champ_date_debut.getAttribute("value").isEmpty());

        // Obtenir la date affichée sur la page
        String dateAffichee = nouveau_projet_champ_date_debut.getAttribute("value");
        // Définir le format de la date attendue
        SimpleDateFormat dateFormatee = new SimpleDateFormat("d MMM yyyy", Locale.FRANCE); // Format "25 janv. 2017"
        // Obtenir la date d'aujourd'hui au format attendu
        Date dateDuJour = new Date();
        String dateDuJourFormattee = dateFormatee.format(dateDuJour);
        // Vérifier si la date affichée correspond à la date d'aujourd'hui et au format attendu
        assertEquals(dateAffichee, dateDuJourFormattee);

        // Echéance : champ de saisie de date associé à un calendrier. Par défaut, le champ n'est pas renseigné.
        assertEquals("Echéance", nouveau_projet_echeance.getText());
        assertTrue(nouveau_projet_champ_echeance.getAttribute("value").isEmpty());

        // Client : Champ de saisie non renseigné
        assertEquals("Client", nouveau_projet_client.getText());
        assertTrue(nouveau_projet_champ_client.getAttribute("value").isEmpty());
        click(nouveau_projet_loupe_client);
        assertTrue(nouveau_projet_liste_client.isDisplayed());
        click(nouveau_projet_loupe_client);

        // Calendrier : Liste déroulante avec pour valeur par défaut "Default"
        assertEquals("Calendrier", nouveau_projet_calendrier.getText());
        assertEquals("Default", nouveau_projet_champ_calendrier.getAttribute("value"));
        click(nouveau_projet_loupe_calendrier);
        assertTrue(nouveau_projet_liste_calendrier.isDisplayed());
        click(nouveau_projet_loupe_calendrier);

        // Boutons [Accepter] et [Annuler]
        assertTrue(nouveau_projet_bouton_accepter.isDisplayed());
        assertTrue(nouveau_projet_bouton_annuler.isDisplayed());

        return PageFactory.initElements(driver, PageProjet.class);
    }

    //////////
    // Méthode qui retourne la date J+5
    public String datePlus5Jours() {
        // Ajouter 5 jours à la date actuelle
        Calendar calendrier = Calendar.getInstance();
        calendrier.setTime(dateActuelle);
        calendrier.add(Calendar.DAY_OF_YEAR, 5); // Ajouter 5 jours
        Date datePlus5Jours = calendrier.getTime();
        // Convertir la date en format "25 janv. 2017"
        SimpleDateFormat dateFormatee = new SimpleDateFormat("d MMM yyyy", Locale.FRANCE);
        String datePlus5JoursFormatee = dateFormatee.format(datePlus5Jours);
        return datePlus5JoursFormatee;
    }

    // Méthode de (sélection dans le calendrier) date J+5 // RV: A choisir sur le calendrier
    public void remplirDatePlus5Jours() throws InterruptedException {
        sendKey(nouveau_projet_champ_date_debut, datePlus5Jours());
    }

    // Méthode qui retourne la date J+15
    public String datePlus15Jours() {
        // Ajouter 15 jours à la date actuelle
        Calendar calendrier = Calendar.getInstance();
        calendrier.setTime(dateActuelle);
        calendrier.add(Calendar.DAY_OF_YEAR, 15); // Ajouter 15 jours
        Date datePlus15Jours = calendrier.getTime();
        // Convertir la date en format "25 janv. 2017"
        SimpleDateFormat dateFormatee = new SimpleDateFormat("d MMM yyyy", Locale.FRANCE);
        String datePlus15JoursFormatee = dateFormatee.format(datePlus15Jours);
        return datePlus15JoursFormatee;
    }

    // Méthode de (sélection dans le calendrier) date J+15 // RV: A choisir sur le calendrier
    public void remplirDatePlus15Jours() throws InterruptedException {
        sendKey(nouveau_projet_champ_echeance, datePlus15Jours());
    }

    // Méthode d'extraction mois de la date début // RV
//    public int[] extraireDate() {
//        // Date date = (Date) datePlus5Jours().chars();
//
//        String dateText = "15 déc. 2023";
//        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("fr", "FR"));
//
//        try {
//            Date date = dateFormat.parse(dateText);
//
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
//
//            int moisD = calendar.get(Calendar.MONTH) + 1; // Ajouter 1 car les mois commencent à partir de 0
//            int anneeD = calendar.get(Calendar.YEAR);
//
//            System.out.println("Mois : " + moisD);
//            System.out.println("Année : " + anneeD);
//
//            return new int[]{moisD, anneeD};
//        } catch (ParseException e) {
//            throw new RuntimeException(e);
//        }
//    }

        // Méthode de création d'un projet - Bouton [Accepter] et vérification du projet créé
    public void creerProjet(WebDriver driver) throws InterruptedException {
        // Saisir Nom : PROJET_TEST1
        sendKey(nouveau_projet_champ_nom, "PROJET_TEST1");

        // Décocher la case "Générer le code" puis saisir dans le champ "Code" la valeur : "PRJTEST001"
        click(nouveau_projet_checkbox_generer_code);
        sendKey(nouveau_projet_champ_code, "PRJTEST001");

        // Date de début : (Sélectionner dans le calendrier) date J+5
        remplirDatePlus5Jours();

        // Echeance : (Sélectionner dans le calendrier) date J+15
        remplirDatePlus15Jours();

        // Cliquer sur le bouton "Accepter"
        click(nouveau_projet_bouton_accepter);

        // Attendre la pop-up disparu
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOf(nouveau_projet_nom));

        // Vérifier l'affichage du menu "Détail du projet" et de l'onglet "WBS (tâches)"
        assertEquals("Détail du projet", projet_menu_vertical.get(1).getText()); // Détail du projet
        assertEquals("WBS (tâches)", projet_onglet_wbs.getText());
    }

    //////////
    // Méthode de vérification les onglets - menu vertical
    public void verifieOrdreMenuVertical() {
        // Liste des onglets attendus dans l'ordre
        List<String> ordreOngletsAttendu = Arrays.asList(
                "Planification de projet",
                "Détail du projet",
                "Chargement des ressources",
                "Allocation avancée",
                "Tableau de bord"
        );
        // Récupérer tous les éléments des onglets du menu vertical
        List<WebElement> ongletsMenuVertical = projet_menu_vertical;

        // Créer une liste pour stocker les textes des onglets
        List<String> ordreOngletsActuel = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement onglet : ongletsMenuVertical) {
            String ongletTexte = onglet.getText().trim();
            if (!ongletTexte.isEmpty()) {
                ordreOngletsActuel.add(ongletTexte);
            }
        }

        // Vérifier si les onglets sont dans l'ordre attendu
        assertEquals(ordreOngletsAttendu, ordreOngletsActuel, "Les onglets ne sont pas dans le bon ordre.");
    }

    //////////
    // Méthode de vérification les onglets - menu horizontal
    public void verifieOrdreMenuHorizontal() {
        // Liste des onglets attendus dans l'ordre
        List<String> ordreOngletsAttendu = Arrays.asList(
                "WBS (tâches)",
                "Données générales",
                "Coût",
                "Avancement",
                "Libellés",
                "Exigence de critère",
                "Matériels",
                "Formulaires qualité des tâches", // "Formulaire qualité des tâches"
                "Autorisation"
        );
        // Récupérer tous les éléments des onglets du menu horizontal
        List<WebElement> ongletsMenuHorizontal = projet_menu_horizontal;

        // Créer une liste pour stocker les textes des onglets
        List<String> ordreOngletsActuel = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement onglet : ongletsMenuHorizontal) {
            String ongletTexte = onglet.getText().trim();
            if (!ongletTexte.isEmpty()) {
                ordreOngletsActuel.add(ongletTexte);
            }
        }

        // Vérifier si les onglets sont dans l'ordre attendu
        assertEquals(ordreOngletsAttendu, ordreOngletsActuel, "Les onglets ne sont pas dans le bon ordre.");
    }

    /////////
    // Méthodes de vérification des boutons d'enregistrement et d'annulation de l'édition du projet
    public void verifieBoutonEnregistrer() {
        //icône représentant une disquette et infobulle associée à l'icône "Enregistrer le projet"
        assertTrue(projet_icone_enregistrer.isDisplayed());
        assertEquals("Enregistrer le projet", projet_infobulle_enregistrer.getAttribute("title"));
    }

    public void verifieBoutonAnnuler() {
        //icône représentant une flèche retour et infobulle associée à l'icône "Annuler l'édition"
        assertTrue(projet_icone_annuler.isDisplayed());
        assertEquals("Annuler l'édition", projet_infobulle_annuler.getAttribute("title"));
    }

    //////////
    // Méthode de clic sur le bouton d'annulation de l'édition du projet et vérification de la pop-up "Confirmer la fenêtre de sortie"
    public void clicAnnulerProjet(WebDriver driver) throws InterruptedException {
        // Cliquer sur l'icône "Annuler l'édition"
        click(projet_icone_annuler);

        // Attendre la pop-up affichée
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//        wait.until(ExpectedConditions.visibilityOf((WebElement) projet_popup_annuler));

        // Vérifier le message
        assertTrue(projet_popup_annuler_contenu.isDisplayed());

        // Vérifier les boutons "OK" et "Annuler"
        assertTrue(projet_popup_annuler_ok.isDisplayed());
        assertTrue(projet_popup_annuler_annuler.isDisplayed());
    }

    // Méthode de clic sur le bouton "Annuler" dans la pop-up "Confirmer la fenêtre de sortie" et vérification de la fermeture de la pop-up
    public void clicAnnulerAnnuler(WebDriver driver) throws InterruptedException {
        // Cliquer sur le bouton "Annuler"
        click(projet_popup_annuler_annuler);

        // Attendre la pop-up disparue
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
//        wait.until(ExpectedConditions.invisibilityOf((WebElement) projet_popup_annuler));

        // Vérifier la fermeture de la pop-up
        assertTrue(projet_popup_annuler.isEmpty()); // RV: Unable to locate element//modif

        // Vérifier l'affichage du menu "Détail du projet" et de l'onglet "WBS (tâches)"
        assertEquals("Détail du projet", projet_menu_vertical.get(1).getText()); // Détail du projet
        assertEquals("WBS (tâches)", projet_onglet_wbs.getText());
    }

    // Méthode de clic sur le bouton "OK" dans la pop-up "Confirmer la fenêtre de sortie" et vérification de la fermeture de la pop-up
    public void clicAnnulerOK(WebDriver driver) throws InterruptedException {
        // Cliquer sur le bouton "Annuler"
        click(projet_popup_annuler_ok);

        // Attendre la pop-up disparue
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//        wait.until(ExpectedConditions.invisibilityOf((WebElement) projet_popup_annuler));

        // Vérifier la fermeture de la pop-up
        assertTrue(projet_popup_annuler.isEmpty()); // RV: Unable to locate element//modif

        // Vérifier l'affichage du menu "Planification des projets" et l'absence de menu horizontal
        assertTrue(projet_menu_horizontal.isEmpty());
        assertTrue(projet_onglet_planification_projets.isDisplayed());
    }

    //////////
    // Méthode de clic sur le logo
    public void clicLogo(WebDriver driver) throws InterruptedException {
        // Cliquer sur l'item "Projet"
        click(logo);
    }

    //////////
    // Méthode de clic sur l'item "Projets" dans l'onglet "Calendrier"
    public void clicProjets(WebDriver driver) throws InterruptedException {
        // Bouger la souris sur l'onglet ressources
        moveMouse(onglet_calendrier);

        // Cliquer sur l'item "Projet"
        click(calendrier_item_projets);

        // Vérifier la liste est affichée à droite du menu vertical, le projet créé y est présent
        assertTrue(projet_liste_projets.isDisplayed());
        assertTrue(projet_nom1.isDisplayed());

        // Vérifier l'affichage de l'onglet "Liste des projets" dans le menu vertical à gauche de la page
        assertTrue(projet_onglet_liste_projets.isDisplayed());
    }

    // Méthode de vérification les informations affichées pour le projet
    public void verifieInfoProjet1() {
        // Nom : PROJET_TEST1
        assertEquals("Nom", projet_liste_projets_nom.getText());
        assertEquals("PROJET_TEST1", projet_nom1.getText());

        // Code : PRJTEST001
        assertEquals("Code", projet_liste_projets_code.getText());
        assertEquals("PRJTEST001", projet_code1.getText());

        // Date de début : date J+5
        assertEquals("Date de début", projet_liste_projets_debut.getText());
        assertEquals(datePlus5Jours(), projet_debut1.getText());

        // Echéance : date J+15
        assertEquals("Echéance", projet_liste_projets_echeance.getText());
        assertEquals(datePlus15Jours(), projet_echeance1.getText());

        // Client : champ non renseigné
        assertEquals("Client", projet_liste_projets_client.getText());
        assertEquals("", projet_client1.getText());

        // Budget total : 0 €
        assertEquals("Budget total", projet_liste_projets_budget.getText());
        assertEquals("0 €", projet_budget1.getText());

        // Heures : 0
        assertEquals("Heures", projet_liste_projets_heures.getText());
        assertEquals("0", projet_heures1.getText());

        // Etat : PRE-VENTES
        assertEquals("Etat", projet_liste_projets_etat.getText());
        assertEquals("PRE-VENTES", projet_etat1.getText());

        // Opérations : 4 icones : modifier, supprimer, voir la prévision, créer un modèle
        assertTrue(projet_icone_modifier1.isDisplayed());
        assertTrue(projet_icone_supprimer1.isDisplayed());
        assertTrue(projet_icone_prevision1.isDisplayed());
        assertTrue(projet_icone_modele1.isDisplayed());
    }

    //////////
    // Méthode de clic sur l'onglet "Liste des projets" dans lel menu vertical à la gauche de la page et vérification de l'affichage de la liste
    public void clicListeProjets(WebDriver driver) throws InterruptedException {
        WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait1.until(ExpectedConditions.elementToBeClickable(projet_onglet_liste_projets));

        // Cliquer sur l'onglet "Liste des projets"
        click(projet_onglet_liste_projets);

        WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait2.until(ExpectedConditions.elementToBeClickable(projet_nom1));

        // Vérifier l'affichage de la liste des projet
        assertTrue(projet_liste_projets.isDisplayed());
    }

    //////////
    // Méthode de clic sur le nom du projet "PROJET_TEST1" dans la liste des projets et vérification de l'affichage de la page
    public void clicProjet1(WebDriver driver) throws InterruptedException {
        // Cliquer sur le nom du projet "PROJET_TEST1"
        click(projet_nom1);

        // Attendre l'affichage de l'édition de projet
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(projet_onglet_wbs));

        // Vérifier l'affichage de l'onglet "WBS (tâches)" du projet
        assertTrue(projet_onglet_wbs.isDisplayed());
    }

    // Méthode de vérification du fil d'ariane affiché
    public void verifieFilAriane() {
        // Fil d'ariane attendu dans l'ordre
        List<String> filAttendu = Arrays.asList(
                "DEBUT",
                "Calendrier",
                "Détail du projet",
                "PROJET_TEST1"
        );
        // Récupérer tous les éléments du fil d'ariane
        List<WebElement> fil = fil_ariane;

        // Créer une liste pour stocker les textes du fil d'ariane
        List<String> filActuel = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement onglet : fil) {
            String ongletTexte = onglet.getText().trim();
            if (!ongletTexte.isEmpty()) {
                filActuel.add(ongletTexte);
            }
        }
        // Vérifier si le fil d'ariane est dans l'ordre attendu
        assertEquals(filAttendu, filActuel, "Le fil d'ariane n'est pas dans le bon ordre.");
    }

    //////////
    // Méthode de création d'une nouvelle tâche
    public void creerTache(WebDriver driver, String tache, String heures) throws RuntimeException, InterruptedException {
        sendKey(projet_wbs_nouvelle_tache, tache);
        sendKey(projet_wbs_heures, heures);
        click(projet_wbs_bouton_ajouter);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.elementToBeClickable(projet_wbs_nouvelle_tache));
    }

    // Méthode de vérification les en-tetes du tableau dans l'onglet "WBS (tâches)"
    public void verifieEntetesWbs() {
        // En-tetes du tableau dans l'onglet "WBS (tâches)" dans l'ordre
        List<String> entetesAttendus = Arrays.asList(
                "Etat de la prévision",
                "Code",
                "Nom",
                "Heures",
                "Budget",
                "Doit débuter après",
                "Echéance",
                "Op."
        );
        // Récupérer tous les éléments des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<WebElement> entetes = projet_wbs_tableau_entetes;

        // Créer une liste pour stocker les textes des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<String> entetesActuels = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement entete : entetes) {
            String enteteTexte = entete.getText().trim();
            if (!enteteTexte.isEmpty()) {
                entetesActuels.add(enteteTexte);
            }
        }
        // Vérifier si les en-tetes sont dans l'ordre attendu
        assertEquals(entetesAttendus, entetesActuels, "Les entetes du tableau dans l'onglet WBS (tâches) " +
                "ne sont pas dans le bon ordre.");
    }

    // Méthode de vérification de l'affichage de la nouvelle ligne de tache
    public void verifieCellules1WBS() {
        // Cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
        // Code: champ vide, Nom: Tache1-P1, Heures: 5, Budget: 0 €, Doit débuter après: champ vide, Echéance: champ vide
        List<String> cellules1Attendus = Arrays.asList(
                "",
                "Tache1-P1",
                "5",
                "0 €",
                "",
                ""
        );
        // Récupérer tous les éléments des cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
        List<WebElement> cellules1 = projet_wbs_tableau_cellules1;

        // Créer une liste pour stocker les textes des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<String> celludles1Actuels = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement cellule1 : cellules1) {
            String cellule1Texte = cellule1.getAttribute("value");
            celludles1Actuels.add(cellule1Texte);
        }
        // Vérifier le contenu des cellules/1ere ligne du tableau dans l'onglet "WBS (tâches)"
        assertEquals(cellules1Attendus, celludles1Actuels);

        // Etat de la prévision : 2 icônes avec les infobulles "Tache1-P1. Avancement:0." et "Déprogrammé", Op. : icône pour modifier et icône pour supprimer
        assertEquals("Tache1-P1.  Avancement:0.", projet_wbs_infobulle_tache1.getAttribute("title"));
        // Absence l'icone "Tache1-P1. Avancement:0."  sur la page et le DOM
        assertEquals("Déprogrammé", projet_wbs_infobulle_deprogramme1.getAttribute("title"));
        assertEquals("http://localhost:8080/libreplan/common/img/ico_unschedule1.png", projet_wbs_icone_deprogramme1.getAttribute("src"));

        assertEquals("http://localhost:8080/libreplan/common/img/ico_editar1.png", projet_wbs_icone_modifier.getAttribute("src"));
        assertEquals("http://localhost:8080/libreplan/common/img/ico_borrar1.png", projet_wbs_icone_supprimer.getAttribute("src"));
    }

    //////////
    // Méthode de vérification de l'affichage des 4 tâches dans l'ordre croissant par rapport à leur création
    public void verifieOrdreTaches1() {
        List<String> listeTachesAttendue = Arrays.asList(
                "Tache1-P1",
                "Tache2-P1",
                "Tache3-P1",
                "Tache4-P1"
        );
        // Récupérer tous les éléments des taches dans l'onglet "WBS (tâches)"
        List<WebElement> taches = projet_wbs_tableau_taches;

        // Créer une liste pour stocker les textes des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<String> listeTachesActuelle = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement tache : taches) {
            String tacheTexte = tache.getAttribute("value");
            listeTachesActuelle.add(tacheTexte);
        }
        // Vérifier si l'ordre des taches est croissant par rapport à leur création.
        assertEquals(listeTachesAttendue, listeTachesActuelle);
    }

    //////////
    // Méthode de modification l'ordre d'affichage des taches en utilisant la flèche descendante
    public void modifOrdreTaches1(WebDriver driver) throws InterruptedException {
        // Cliquer sur le nom du projet "PROJET_TEST1"
        projet_wbs_tache1.click();
        click(projet_wbs_descendre_tache);
    }

    // Méthode de vérification de l'affichage des 4 tâches (La tâche "Tache1-P1" est affichée sous de la tâche "Tache2-P1")
    public void verifieOrdreTaches2() {
        List<String> listeTachesAttendue = Arrays.asList(
                "Tache2-P1",
                "Tache1-P1",
                "Tache3-P1",
                "Tache4-P1"
        );
        // Récupérer tous les éléments des taches dans l'onglet "WBS (tâches)"
        List<WebElement> taches = projet_wbs_tableau_taches;

        // Créer une liste pour stocker les textes des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<String> listeTachesActuelle = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement tache : taches) {
            String tacheTexte = tache.getAttribute("value");
            listeTachesActuelle.add(tacheTexte);
        }
        // Vérifier si l'ordre des taches est croissant par rapport à leur création.
        assertEquals(listeTachesAttendue, listeTachesActuelle);
    }

    //////////
    // Méthode de modification l'ordre d'affichage des taches en utilisant la flèche montante
    public void modifOrdreTaches2(WebDriver driver) throws InterruptedException {
        // Cliquer sur le nom du projet "PROJET_TEST1"
        projet_wbs_tache3.click();
        click(projet_wbs_monte_tache);
    }

    // Méthode de vérification de l'affichage des 4 tâches (La tâche "Tache3-P1" est affichée sous de la tâche "Tache2-P1") // RV: Résultat attendu à corriger?
    public void verifieOrdreTaches3() {
        List<String> listeTachesAttendue = Arrays.asList(
                "Tache2-P1",
                "Tache3-P1",
                "Tache1-P1",
                "Tache4-P1"
        );
        // Récupérer tous les éléments des taches dans l'onglet "WBS (tâches)"
        List<WebElement> taches = projet_wbs_tableau_taches;

        // Créer une liste pour stocker les textes des en-tetes du tableau dans l'onglet "WBS (tâches)"
        List<String> listeTachesActuelle = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement tache : taches) {
            String tacheTexte = tache.getAttribute("value");
            listeTachesActuelle.add(tacheTexte);
        }
        // Vérifier si l'ordre des taches est croissant par rapport à leur création.
        assertEquals(listeTachesAttendue, listeTachesActuelle);
    }

    /////////
    // Méthode de renseignement les informations des taches
    public void remplirTaches(WebDriver driver) throws InterruptedException {
        // Liste de textes à envoyer dans les cellules // RV: A envoyer MM/AAAA en fonction des dates renseignées pour le projet (date de début et date d'échéance)
        List<String> texte1 = Arrays.asList(
                "T2", "08/01/24", ""
        );

        List<String> texte2 = Arrays.asList(
                "T3", "", "03/01/24"
        );

        List<String> texte3 = Arrays.asList(
                "T1", "05/01/24", ""
        );

        List<String> texte4 = Arrays.asList(
                "T4", "", "05/01/24"
        );

        // Récupérer toutes les cellules de la ligne
        List<WebElement> cellules1 = projet_wbs_tableau_cellules1;
        List<WebElement> cellules2 = projet_wbs_tableau_cellules2;
        List<WebElement> cellules3 = projet_wbs_tableau_cellules3;
        List<WebElement> cellules4 = projet_wbs_tableau_cellules4;

        // Compteur pour le texte statique à insérer
        int textIndex = 0;

        // Parcourir chaque cellule et remplir avec du texte statique
        for (WebElement cellule : cellules1) {
            String celluleTexte = cellule.getAttribute("value");
            if (celluleTexte.isEmpty()) {
                cellule.sendKeys(texte1.get(textIndex % texte1.size()));
                textIndex++;
            }
        }

        for (WebElement cellule : cellules2) {
            String celluleTexte = cellule.getAttribute("value");
            if (celluleTexte.isEmpty()) {
                cellule.sendKeys(texte2.get(textIndex % texte2.size()));
                textIndex++;
            }
        }

        for (WebElement cellule : cellules3) {
            String celluleTexte = cellule.getAttribute("value");
            if (celluleTexte.isEmpty()) {
                cellule.sendKeys(texte3.get(textIndex % texte3.size()));
                textIndex++;
            }
        }

        for (WebElement cellule : cellules4) {
            String celluleTexte = cellule.getAttribute("value");
            if (celluleTexte.isEmpty()) {
                cellule.sendKeys(texte4.get(textIndex % texte4.size()));
                textIndex++;
            }
        }

    }

    // Méthode de clic sur l'icône d'enregistrement du projet (disquette) et vérification de l'affichage de la pop-up "Information"
    public void clicEnregistrer(WebDriver driver) throws InterruptedException {
        // Cliquer sur l'icône d'enregistrement du projet (disquette)
        click(projet_icone_enregistrer);

        WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait1.until(ExpectedConditions.visibilityOf(projet_enregistrer_message));

        // Vérifier l'affichage de la pop-up "Information"
        // message : "Projet enregistré"
        assertTrue(projet_enregistrer_message.isDisplayed());
        // bouton "OK"
        assertTrue(projet_enregistrer_bouton_OK.isDisplayed());
        // bouton permettant de fermer la pop-up (x)
        assertTrue(projet_enregistrer_bouton_x.isDisplayed());

        // Cliquer sur le bouton OK
        click(projet_enregistrer_bouton_OK);

        WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait2.until(ExpectedConditions.visibilityOf(projet_menu_vertical.get(0))); // Planification du projet
    }

    /////////
    // Méthode de clic sur l'onglet "Planification de projet" dans le menu vertical et vérification de l'affichage des taches et du planning
    public void clicPlanificationProjet(WebDriver driver) throws InterruptedException {
        click(projet_menu_vertical.get(0)); // Planification du projet

        // Vérifier l'affichage de barres horizontales (bleu clair) pour représenter Date début à Date fin
        // Parcourir chaque élément représentant une barre horizontale
        for (WebElement bar : projet_planification_barres_horizontales1) {
            // Récupérer la valeur de la propriété 'background-color' du style CSS de l'élément
            String backgroundColor= bar.getCssValue("background-color");

            // Vérifier si la couleur est bleu clair (ou une autre couleur spécifique)
            assertTrue(backgroundColor.equals("#abcde1"));
        }

        // Vérifier l'affichage de barres verticales (noir) pour représenter les échéances
        // Parcourir chaque élément représentant une barre horizontale
        for (WebElement bar : projet_planification_barres_verticales) {
            // Récupérer la valeur de la propriété 'background-image' du style CSS de l'élément
            String backgroundImage= bar.getCssValue("background-image");

            // Vérifier si la couleur est bleu clair (ou une autre couleur spécifique)
            assertTrue(backgroundImage.equals("http://localhost:8080/libreplan/zkau/web/ganttz/img/deadline.png"));
        }
    }

    ////////
    // Méthode de se positionner sur la barre horizontale bleue puis effectuer un clic droit avec la souris
    public void clicDroitBarre1(WebDriver driver) throws InterruptedException {
        // Bouger la souris sur la barre horizontale
        moveMouse(projet_tache1_barre);

        // Effectuer un clic droit
        Actions actions = new Actions(driver);
        actions.contextClick(projet_tache1_barre).perform();
    }

    // Méthode de vérification l'affichage d'une liste d'actions possibles sur la tâche
    /*"Ajouter une dépendance", "Ajouter un jalon", "Propriétés de la tâche", "Allocation de ressources",
    "Allocation avancée", "Sous-contrat", "Allocation de calendrier", "Affectation d'avancement", "Consolidation d'avancement"
     */
    public void verifieListeActionsTache1() {
        List<String> listeActionsAttendue = Arrays.asList(
                "Ajouter une dépendance",
                "Ajouter un jalon",
                "Propriétés de la tâche",
                "Allocation de ressources",
                "Allocation avancée",
                "Sous-contrat",
                "Allocation de calendrier",
                "Affectation d'avancement",
                "Consolidation d'avancement"
        );
        // Récupérer tous les éléments des actions de la tache/1ere ligne dans l'onglet "Planification de projet"
        List<WebElement> actions = projet_tache1_liste_actions;

        // Créer une liste pour stocker les actions
        List<String> listeActionsActuelle = new ArrayList<>();

        // Récupérer les textes des onglets
        for (WebElement action : actions) {
            String actionTexte = action.getText();
            listeActionsActuelle.add(actionTexte);
        }
        // Vérifier si l'ordre des taches est croissant par rapport à leur création.
        assertEquals(listeActionsAttendue, listeActionsActuelle);
    }

    // Méthode de clic sur "Allocation de ressources" et vérification de l'ouverture d'une pop-up "Modifier la tâche : XXX"
    public void clicAllocation(WebDriver driver) throws InterruptedException {
        // Bouger la souris sur la barre horizontale du Tache2-P1
        moveMouse(projet_tache1_action_allocation_ressources);

        // Cliquer sur "Allocation de ressources"
        click(projet_tache1_action_allocation_ressources);

        WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait2.until(ExpectedConditions.visibilityOf(projet_tache1_action_allocation_ressources_onglet2));

        // Vérifier l'ouverture de la pop-up "Modifier la tâche : Tache2-P1"
        assertFalse(projet_tache1_action_allocation_ressources_popup.isEmpty());

        // Onglet "Propriétés de la tâche"
        assertTrue(projet_tache1_action_allocation_ressources_onglet1.isDisplayed());
        // Onglet "Allocation de ressource normale"
        assertTrue(projet_tache1_action_allocation_ressources_onglet2.isDisplayed());
        // Bouton [Accepter]
        assertTrue(projet_tache1_action_allocation_ressources_onglet2_bouton_accepter.isDisplayed());
        // Bouton [Annuler]
        assertTrue(projet_tache1_action_allocation_ressources_onglet2_bouton_annuler.isDisplayed());
        // Onglet "Allocation de ressource normale" affiché
        assertTrue(projet_tache1_action_allocation_ressources_onglet2_bouton_accepter.isEnabled());
    }

    // Méthode de clic sur la loupe associée au champ de saisie "Choisir les critères ou les ressources" et vérification de l'affichage d'une liste déroulante
    public void clicLoupeRessource(WebDriver driver) throws InterruptedException {
        // Cliquer sur la loupe associée au champ de saisie "Choisir les critères ou les ressources"
        click(projet_tache1_action_allocation_ressources_onglet2_loupe);

        // Vérifier l'affichage d'une liste déroulante // RV: Affichage d'une liste déroulante contenant toutes les ressources qui existent en base de données
        assertFalse(projet_tache1_action_allocation_ressources_onglet2_liste.isEmpty());
    }

    /* Méthode de clic sur une ressource/1ere dans la liste déroulante associée au champ de saisie "Choisir les critères ou les ressources"
    et vérification de l'affichage de la ressource sélectionnée dans le champ de saisie "Choisir les critères ou les ressources"
    et la liste déroulante
     */
    public void clicRessource1(WebDriver driver) throws InterruptedException {
        // Cliquer sur une ressource/1ere ligne
        click(projet_tache1_action_allocation_ressources_onglet2_ressources.get(0));

        // Vérifier l'affichage de la ressource sélectionnée dans le champ de saisie "Choisir les critères ou les ressources" // RV

        // Vérifier la fermeture de la liste déroulante
        assertTrue(projet_tache1_action_allocation_ressources_onglet2_liste.isEmpty());
    }

    // Méthode de clic sur le bouton "Ajouter" et vérification de l'affichage de la ressource sélectionnée
    public void clicAjouterRessource(WebDriver driver) throws InterruptedException {
        click(projet_tache1_action_allocation_ressources_onglet2_bouton_ajouter);
    }

    // Méthode de récupérer le texte/ nom de la ressource/ 1ere ligne
    public String recupererNom1() {
        nom1 = projet_tache1_action_allocation_ressources_onglet2_ressources.get(0).getText();
        System.out.println(nom1);
        return nom1;
    }

    // Méthode de vérification de l'affichage de la ressource/son nom dans le tableau Allocations
    public void verifieRessource() {
        for (WebElement nom : projet_tache1_action_allocation_ressources_onglet2_tableau2) {
            // Récupérer le texte/ nom dans le tableau
            String texte = nom.getText();

            // Vérifier la présence du nom (nom1) dans le texte de l'élément
            assertTrue(texte.contains(recupererNom1()));
        }
    }

    // Méthode de clic sur le bouton "Accepter" et vérification de la fermeture la pop-up et de l'affichage de la barre horizontale dans le planning
    public void clicAccepter(WebDriver driver) throws InterruptedException {
        click(projet_tache1_action_allocation_ressources_onglet2_bouton_accepter);

        WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait1.until(ExpectedConditions.invisibilityOf(projet_tache1_action_allocation_ressources_onglet2));

        WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait2.until(ExpectedConditions.visibilityOf(projet_menu_vertical.get(2)));

        // Vérifier la fermeture de la pop-up
        assertTrue(projet_tache1_action_allocation_ressources_popup.isEmpty());

        // Vérifier si la barre horizontale associée à la tâche pour laquelle une ressource vient d'être allouée n'est plus de la même couleur (bleu plus foncé)
        // Parcourir chaque élément représentant une barre horizontale
        for (WebElement bar : projet_planification_barres_horizontales2) {
            // Récupérer la valeur de la propriété 'background-color' du style CSS de l'élément
            String backgroundColor= bar.getCssValue("background-color");

            // Vérifier si la couleur est bleu clair (ou une autre couleur spécifique)
            assertTrue(backgroundColor.equals("#3c90be"));
        }
    }

    // Méthode de clic sur l'onglet "Chargement des ressources" dans le menu vertical et vérification de l'affichage de la ressource
    public void clicChargementRessources(WebDriver driver) throws InterruptedException {
        click(projet_menu_vertical.get(2));

//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
//        wait.until(ExpectedConditions.visibilityOfAllElements(projet_noms_ressources));

        // Vérifier l'affichage de la ressource
        for (WebElement nom : projet_noms_ressources) {
            // Récupérer le texte/ nom dans la liste des noms de ressources
            String texte = nom.getText();

            // Vérifier la présence du nom (nom1) dans le texte de l'élément
            assertTrue(texte.contains(nom1));
        }

        // Vérifier si la charge de la ressource est représentée sur le planning par une barre horizontale jaune
        // Parcourir chaque élément représentant une barre horizontale
        for (WebElement bar : projet_ressources_barres_horizontales) {
            // Récupérer la valeur de la propriété 'background-color' du style CSS de l'élément
            String backgroundColor= bar.getCssValue("background-color");

            // Vérifier si la couleur est bleu clair (ou une autre couleur spécifique)
            assertTrue(backgroundColor.equals("#fdbe13"));
        }
    }

    // Méthode de clic sur l'icône affichée avant le nom/ 1ere ligne de la ressource dans l'onglet "Chargement des ressources" et vérification de l'affichage de l'arborescence
    public void clicIconeNom1(WebDriver driver) throws InterruptedException {
        click(projet_noms_ressources.get(0));

        // Vérifier l'affichage de l'arborescence: Nom de la ressource - Nom de la tâche/ 1ere ligne
        assertTrue(projet_noms_ressources.get(2).isDisplayed());
    }

    // Méthode de re-clic sur l'icône affichée avant le nom/ 1ere ligne de la ressource dans l'onglet "Chargement des ressources" et vérification de l'affichage de l'arborescence
    public void reClicIconeNom1(WebDriver driver) throws InterruptedException {
        click(projet_noms_ressources.get(0));

        // Vérifier l'affichage de l'arborescence: Nom de la ressource - Nom de la tâche/ 1ere ligne
        assertFalse(projet_noms_ressources.get(2).isDisplayed());
    }

    /////////
    // Méthode de clic sur la valeur "Année" dans "Zoom" et vérification de l'affichage des années
    public void clicAnnee(WebDriver driver) throws InterruptedException {
        click(projet_zoom);
        click(projet_zoom_options.get(0));

        // Vérifier l'affichage des années: H1 et H2
        for (WebElement annee : projet_affichage_annees) {
            // Récupérer le texte/ nom dans la liste des noms de ressources
            String texte = annee.getText();

            // Vérifier la présence du texte de l'élément
            assertTrue(texte.contains("H1") || texte.contains("H2"));
        }
    }

    // Méthode de clic sur la valeur "Trimestre" dans "Zoom" et vérification de l'affichage des années
    public void clicTrimestre(WebDriver driver) throws InterruptedException {
        click(projet_zoom);
        click(projet_zoom_options.get(1));

        // Créer une liste pour stocker les textes des années
        List<String> annees = new ArrayList<>();

        // Vérifier l'affichage des années: Q1, Q2, Q3 et Q4
        for (WebElement annee : projet_affichage_annees) {
            // Récupérer le texte/ nom dans la liste des noms de ressources
            String texte = annee.getText().trim();
            annees.add(texte);
            //System.out.println(annees);

            // Vérifier la présence du texte de l'élément
            assertTrue(annees.contains("Q1") || annees.contains("Q2") || annees.contains("Q3") || annees.contains("Q4"));
        }
    }

    // Méthode de clic sur la valeur "Mois" dans "Zoom" et vérification de l'affichage des années
    public void clicMois(WebDriver driver) throws InterruptedException {
        click(projet_zoom);
        click(projet_zoom_options.get(2));

        // Créer une liste pour stocker les textes des années
        List<String> annees = new ArrayList<>();

        // Vérifier l'affichage des années: Q1, Q2, Q3 et Q4
        for (WebElement annee : projet_affichage_annees) {
            // Récupérer le texte/ nom dans la liste des noms de ressources
            String texte = annee.getText().trim();
            annees.add(texte);

            // Vérifier la présence du texte de l'élément
            assertTrue(annees.contains("janv.") || annees.contains("févr.") || annees.contains("mars")
                    || annees.contains("avr.") || annees.contains("mai") || annees.contains("juin")
                    || annees.contains("juil.") || annees.contains("août") || annees.contains("sept.")
                    || annees.contains("oct.") || annees.contains("nov.") || annees.contains("déc."));
        }
    }

    // Méthode de supprimer le projet
    public void supprimerProjet(WebDriver driver) throws InterruptedException {
        clicProjets(driver);
        click(projet_icone_supprimer1);
        click(projet_supprimer1_ok);
    }
}
