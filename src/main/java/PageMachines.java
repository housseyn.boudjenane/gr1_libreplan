import Outils.PageOutils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.v85.page.Page;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PageMachines extends PageOutils {

    @FindBy(xpath="//button[contains(@id, 'r-b')]")
    WebElement ongletRessources;

    @FindBy(xpath="//a[contains(@id,'u-a')]")
    WebElement boutonMachines;

    @FindBy(xpath="//div[contains(@id,'4-cap') and text()='Machines Liste']")
    WebElement headerMachinesListe;

    @FindBy(xpath="//div[contains(@id,'s5-cave')]")
    WebElement celluleNom;

    @FindBy(xpath="//div[contains(@id,'t5-cave')]")
    WebElement celluleDescription;

    @FindBy(xpath="//div[contains(@id,'u5-cave')]")
    WebElement celluleCode;

    @FindBy(xpath="//div[contains(@id,'v5-cave')]")
    WebElement celluleEnFile;

    @FindBy(xpath="//div[contains(@id,'w5-cave')]")
    WebElement celluleOperations;

    @FindBy(xpath="//span[@title=\"Choisir l'ensemble requis de critères et appuyer sur le bouton filtrer\"]")
    WebElement texteFiltrePar;

    @FindBy(xpath="//input[contains(@id,'4-real')]")
    WebElement champRechercheFiltre;

    @FindBy(xpath="//span[contains(@id,'c5')]")
    WebElement texteDetailsPersonnels;

    @FindBy(xpath="//input[contains(@id,'d5')]")
    WebElement champDetailsPersonnels;

    @FindBy(xpath="//td[contains(@id,'5-cnt')]")
    WebElement boutonPlusDoptions;

    @FindBy(xpath="//td[text()='Filtre']")
    WebElement boutonFiltre;

    @FindBy(xpath="//table[contains(@id,'x5-box')]")
    WebElement boutonCreer;

    @FindBy(xpath="//td[contains(@id,'5-cnt') and text()='Créer Machine']")
    WebElement headerCreerMachine;

    @FindBy(xpath = "//span[text()='Donnée de la machine']/ancestor::li")
    WebElement ongletDonneeDeLaMachine;

    @FindBy(xpath="//input[contains(@id,'g6')]")
    WebElement champCodeNonModifiable;

    @FindBy(xpath="//input[contains(@id,'h6-real')]")
    WebElement caseGenererCode;

    @FindBy(xpath="//input[contains(@id,'n6')]")
    WebElement champDescriptionVide;

    @FindBy (xpath="//input[contains(@id,'k6')]")
    WebElement champNomVide;

    @FindBy(xpath="//td[contains(@class, 'z-button-cm') and text()='Enregistrer']")
    WebElement boutonEnregistrer;

    @FindBy(xpath="//td[contains(@class, 'z-button-cm') and text()='Sauver et continuer']")
    WebElement boutonSauver;

    @FindBy(xpath="//span[contains(@id, 'd') and contains(@class, 'cancel-button')]//td[contains(@class, 'z-button-cm') and contains(text(), 'Annuler')]")
    WebElement boutonAnnuler;

    @FindBy(xpath="//select[contains(@id,'6')]")
    WebElement listeDeroulanteParDefaut;

    @FindBy(xpath="//input[contains(@id,'g6')]")
    WebElement champCode;

    @FindBy(xpath="//div[contains(@class, 'message_INFO')]//span[contains(@class, 'z-label')]")
    WebElement messageValidationEnregistre;

    @FindBy(xpath="//td[contains(@id,'5-cnt') and text()='Modifier Machine: MACHINETEST1']")
    WebElement headerMessageValidation;

    @FindBy(xpath="//span[contains(text(), 'MACHINETEST1')]/ancestor::tr//img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement iconePoubelle;

    @FindBy(xpath="//td[contains(text(),'OK')]")
    WebElement boutonOkSuppression1;

    @FindBy(xpath="//div[contains(@class, 'message_INFO')]//span[contains(@class, 'z-label')]")
    WebElement messageSuppressionEnregistre;


    // Constructeur de la classe, appelant également le constructeur de la classe parente (PageOutils)
    public PageMachines(WebDriver driver) {
        super(driver);
        // Initialisation des éléments de la page à l'aide de la méthode myPageFactory
        myPageFactory(driver, this);
    }



    public void creerMachine() throws InterruptedException {

        // *** PAS DE TEST N°2 ***

        // Déplacer la souris sur l'onglet des ressources et cliquer sur le bouton Machines
        moveMouse(ongletRessources);
        click(boutonMachines);

        //Assert du header de la page
        String contenuHeaderMachinesListe = headerMachinesListe.getText();
        assertEquals("Machines Liste", contenuHeaderMachinesListe);

        // Assertions pour vérifier les en-têtes de chaque colonne dans le tableau des machines
        String assertNom = celluleNom.getText();
        assertEquals("Nom", assertNom);

        String assertDescription = celluleDescription.getText();
        assertEquals("Description", assertDescription);

        String assertCode = celluleCode.getText();
        assertEquals("Code", assertCode);

        String assertEnFile = celluleEnFile.getText();
        assertEquals("En file", assertEnFile);

        String assertOperations = celluleOperations.getText();
        assertEquals("Opérations", assertOperations);

        // Vérifier la visibilité de divers éléments sur la page
        assertTrue(texteFiltrePar.isDisplayed());
        assertTrue(champRechercheFiltre.isDisplayed());
        assertTrue(texteDetailsPersonnels.isDisplayed());
        assertTrue(champDetailsPersonnels.isDisplayed());
        assertTrue(boutonPlusDoptions.isDisplayed());
        assertTrue(boutonFiltre.isDisplayed());
        assertTrue(boutonCreer.isDisplayed());

        // Cliquer sur le bouton créer
        click(boutonCreer);

        // *** PAS DE TEST N°3 ***

        //Assert du header de la page
        String assertHeaderCreer = headerCreerMachine.getText();
        assertEquals("Créer Machine", assertHeaderCreer);

        // Assert de l'onglet sélectionné par défaut
        boolean assertOngletSelectionne = ongletDonneeDeLaMachine.getAttribute("class").contains("z-tab-seld");
        assertTrue(assertOngletSelectionne, "L'onglet 'Donnée de la machine' n'est pas sélectionné par défaut");


        // *** PAS DE TEST N°4 ***

        // Vérifier si le champ de code est désactivé
        String disabledAttribute = champCodeNonModifiable.getAttribute("disabled");
        if (disabledAttribute == null || disabledAttribute.isEmpty()) {
            throw new AssertionError("Le champ de saisie est modifiable");
        }

        // Vérifier si la case à cocher pour générer un code est sélectionnée par défaut
        if (!caseGenererCode.isSelected()) {
            throw new AssertionError("La case à cocher n'est pas cochée par défaut");
        }

        // Vérifier que les champs de prénom, nom et ID sont vides par défaut
        String champPrenomReel = champDescriptionVide.getText();
        assertTrue(champPrenomReel.isEmpty());

        String champNomReel = champNomVide.getText();
        assertTrue(champNomReel.isEmpty());

        // Vérifier la valeur par défaut de la liste déroulante pour le type de ressource
        Select selectListeDeroulante = new Select(listeDeroulanteParDefaut);
        WebElement optionSelectionnee = selectListeDeroulante.getFirstSelectedOption();
        String texteOptionSelectionnee = optionSelectionnee.getText();
        String valeurAttendue = "Ressource normale";
        assertEquals(valeurAttendue, texteOptionSelectionnee, "La valeur par défaut de la liste déroulante n'est pas la bonne");

        // Vérifier la visibilité des trois boutons
        assertTrue(boutonEnregistrer.isDisplayed());
        assertTrue(boutonSauver.isDisplayed());
        assertTrue(boutonAnnuler.isDisplayed());

        // *** PAS DE TEST N° 5 ***

        // Cliquer sur la case "Générer le code" pour pouvoir y saisir du texte
        click(caseGenererCode);
        champCode.clear();
        sendKey(champCode,"MACHINETEST1");
        sendKey(champNomVide,"MACHINETEST1");
        sendKey(champDescriptionVide,"MACHINETEST1");
        click(boutonSauver);
        Thread.sleep(5);

        // Assertion du message de validation
        String assertMessageValidation = messageValidationEnregistre.getText();
        assertEquals("Machine \"MACHINETEST1\" enregistré", assertMessageValidation);

        String assertHeaderValidation = headerMessageValidation.getText();
        assertEquals("Modifier Machine: MACHINETEST1", assertHeaderValidation);

        //Cliquer sur le bouton Annuler
        click(boutonAnnuler);
        assertEquals("Machines Liste", contenuHeaderMachinesListe);

        //Suppression de la ligne pour pouvoir rejouer le test
        click(iconePoubelle);
        click(boutonOkSuppression1);




    }
}