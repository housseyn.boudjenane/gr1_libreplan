import Outils.PageOutils;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PageTypeAvancement extends PageOutils {

    // WebElement représentant l'onglet "Ressources"
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Ressources')]")
    WebElement onglet_Ressources;


    // WebElement représentant l'item "Types Avancement"
    @FindBy(xpath = "//a[@href='/libreplan/advance/advanceTypes.zul']")
    WebElement item_typeAvancement;

    // WebElement représentant le boutton créer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer']")
    WebElement boutton_creer;


    // WebElement représentant le titre de la page "Types d'avancement Liste"
    @FindBy(xpath = "//div[@class='z-window-embedded-header' and text() = \"Types d'avancement Liste\"]")
    WebElement title_typeAvancement;

    // WebElement représentant le titre de colonne "Nom"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Nom')]")
    WebElement typeAvancement_nom;

    // WebElement représentant le titre de colonne "Activé"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Activé')]")
    WebElement typeAvancement_active;

    // WebElement représentant le titre de colonne "Prédéfini"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Prédéfini')]")
    WebElement typeAvancement_predefini;

    // WebElement représentant le titre de colonne "Opérations"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Opérations')]")
    WebElement typeAvancement_operations;

    // WebElement représentant le titre de la page "Créer Type avancement"
    @FindBy(xpath = "//td[@class='z-caption-l' and text() = \"Créer Type d'avancement\"]")
    WebElement title_creer;

    // WebElement représentant le boutton "Enregistrer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer']")
    WebElement boutton_enregistrer;

    // WebElement représentant le boutton "Sauver et continuer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Sauver et continuer']")
    WebElement boutton_sauver;

    // WebElement représentant le boutton "Annuler"
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Annuler']")
    WebElement boutton_annuler;

    // WebElement représentant l'onglet "Modifier"
    @FindBy(xpath = "//span[@class='z-tab-text' and text() = 'Modifier']")
    WebElement onglet_modif;

    //WebElement représentant case "Nom"
    @FindBy(xpath = "//input[contains(@id,'55')]")
    WebElement case_nom;

    // WebElement représentant la checkbox "Actif"
    @FindBy(xpath = "//input[contains(@id,'85-real')]")
    WebElement checkbox_actif;

    //WebElement représentant "Valeur maximale par défaut"
    @FindBy(xpath = "//input[contains(@id,'b5')]")
    WebElement val_max;

    //WebElement représentant "Précision"
    @FindBy(xpath = "//input[contains(@id,'e5')]")
    WebElement precision;

    //WebElement représentant "Pourcentage"
    @FindBy(xpath = "//input[contains(@id,'k5')]")
    WebElement pourcentage;

    //WebElement représentant le message de confirmation de création
    @FindBy(xpath = "//div[@class='message_INFO']")
    WebElement message_confirm;

    //WebElement représentant le nom dans la colonne nom du Type d'avancement créer
    @FindBy(xpath = "//span[@class='z-label' and text() = 'Type avancement - Test 1']")
    WebElement nom_test1;

    //WebElement représentant la check box "activé" pour le type créer
    @FindBy(xpath = "//span[@class='z-label' and text()='Type avancement - Test 1']/ancestor::tr/td[2]/div/span/input")
    WebElement actif_test1;

    @FindBy(xpath = "//span[@class='z-label' and text()='Type avancement - Test 1']/ancestor::tr/td[3]/div/span/input")
    WebElement predef_test1;
    ////input[contains(@id,'m6-real')]

    //WebElement représentant l'icône "Modifier" du type créer
    @FindBy(xpath = "//tr/td[contains(@id,'19')]//*/img[@src='/libreplan/common/img/ico_editar1.png']")
    WebElement icone_modif_test1;

    //WebElement représentant l'icône supprimer du type créer
    @FindBy(xpath = "//tr/td[contains(@id,'29')]//*/img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement icone_supp_test1;

    //WebElement du titre apres modification
    @FindBy(xpath = "//td[contains(@id,'t4-cnt')]")
    WebElement title_modif;


    //WebElement représentant le nom dans la colonne nom du Type d'avancement créer
    @FindBy(xpath = "//span[@class='z-label' and text() = 'Type avancement - Test 2']")
    WebElement nom_test2;

    //WebElement représentant la check box "activé" pour le type créer
    @FindBy(xpath = "//span[@class='z-label' and text()='Type avancement - Test 2']/ancestor::tr/td[2]/div/span/input")
    WebElement actif_test2;

    @FindBy(xpath = "//span[@class='z-label' and text()='Type avancement - Test 2']/ancestor::tr/td[3]/div/span/input")
    WebElement predef_test2;

    //WebElement représentant l'icône "Modifier" du type créer
    @FindBy(xpath = "//tr/td[contains(@id,'j8')]//*/img[@src='/libreplan/common/img/ico_editar1.png']")
    WebElement icone_modif_test2;

    //WebElement représentant l'icône supprimer du type créer
    @FindBy(xpath = "//tr/td[contains(@id,'k8')]//*/img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement icone_supp_test2;

    @FindBy(xpath = "//td[contains(text(),'OK')]")
    WebElement button_OK;

    @FindBy(xpath = "//tr/td[contains(@id,'z6')]//*/img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement supp_autre;



    public PageTypeAvancement(WebDriver driver) {
        super(driver);
        myPageFactory(driver, this);
    }

    // Méthode pour Créer un calendrier
    public void creerTypeAvancement(WebDriver driver) throws InterruptedException {
        // Passer la souris sur l'onglet "Ressources" puis dans le sous-menu qui s'affiche, cliquer sur l'item "Types d'avancement"
        moveMouse(onglet_Ressources);
        click(item_typeAvancement);

        // Assert que le titre de la page "Liste des Types d'avancement" est affiché
        assertTrue(title_typeAvancement.isDisplayed());

        // Assert que les colonnes "Nom" "Activé" "Prédéfini" "Opérations" sont affichés
        assertTrue(typeAvancement_nom.isDisplayed());
        assertTrue(typeAvancement_active.isDisplayed());
        assertTrue(typeAvancement_predefini.isDisplayed());
        assertTrue(typeAvancement_operations.isDisplayed());

        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        // Cliquer sur le bouton "Créer"
        click(boutton_creer);

        // Assert que le titre de la page "Créer Type d'avancement" est affiché
        assertTrue(title_creer.isDisplayed());

        // Assert que l'onglet est "Modifier" est affiché
        assertTrue(onglet_modif.isDisplayed());

        // Assert que le champs Nom d'unité est non renseigné
        assertEquals("",case_nom.getText());

        // Assert que la case Actif est cochée par défaut
        assertTrue(checkbox_actif.isSelected());

        //Assert que le champs Valeur maximum par defaut a une valeur par defaut de "100,00" dans le champs de saisie
        assertEquals("100,00",val_max.getAttribute("value"));

        //Assert que le champs Précision a une valeur par défaut  de "0,1000" dans le chaps de saisie
        assertEquals("0,1000",precision.getAttribute("value"));

        //Assert que le champs Type a une Valeur non modifiable "User"

        //Assert que  le champs Pourcentage a une case à cocher décochée par défaut
        assertFalse(pourcentage.isSelected());
        // Assert que les boutons "Enregistrer" "Sauver et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());


        //Saisie "Type avancement - Test 1" dans nom d'unité
        sendKey(case_nom,"Type avancement - Test 1");

        //Décocher la case Actif
        checkbox_actif.click();

        //Saisie "10,00" dans valeur maximum par défaut
        sendKey(val_max,"10,00");

        //Cliquer sur Enregistrer
        boutton_enregistrer.click();

        //Controle du message de confirmation de création
        assertEquals(message_confirm.getText(),"Type d'avancement \"Type avancement - Test 1\" enregistré");

        //Vérification précense ligne créer "Nom" "Actif" "Predéfini" et les icone modif et supprimer
        assertEquals(nom_test1.getText(), "Type avancement - Test 1");
        assertFalse(actif_test1.isEnabled());
        assertFalse(predef_test1.isEnabled());
        assertTrue(icone_modif_test1.isDisplayed());
        assertTrue(icone_supp_test1.isDisplayed());

        // Cliquer sur le bouton "Créer"
        click(boutton_creer);


        // Assert que le titre de la page "Liste des Types d'avancement" est affiché
        assertTrue(title_creer.isDisplayed());

        //Saisie "Type avancement - Test 1" dans nom d'unité
        sendKey(case_nom,"Type avancement - Test 2");

        //Clique sur "Pourcentage"
        pourcentage.click();

        //Vérification que le champs valeur maximum par défaut est grisé et non modifiable
        assertFalse(val_max.isEnabled());

        //Clique sur "Sauver et continuer"
        boutton_sauver.click();

        //Vérification présence message d'information pour confirmer création
        assertEquals(title_modif.getText(),"Modifier Type d'avancement: Type avancement - Test 2");

        // Cliquer sur le bouton [Annuler]
        click(boutton_annuler);

        // Assert que le titre de la page "Liste des Types d'avancement" est affiché
        assertTrue(title_typeAvancement.isDisplayed());

        // Assert que les colonnes "Nom" "Activé" "Prédéfini" "Opérations" sont affichés
        assertTrue(typeAvancement_nom.isDisplayed());
        assertTrue(typeAvancement_active.isDisplayed());
        assertTrue(typeAvancement_predefini.isDisplayed());
        assertTrue(typeAvancement_operations.isDisplayed());

        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        //Vérification précense ligne créer "Nom" "Actif" "Predéfini" et les icone modif et supprimer
        assertEquals(nom_test2.getText(), "Type avancement - Test 2");
        assertFalse(actif_test2.isEnabled());
        assertFalse(predef_test2.isEnabled());
        assertTrue(icone_modif_test2.isDisplayed());
        assertTrue(icone_supp_test2.isDisplayed());

        //Clean de nos données
        icone_supp_test2.click();
        button_OK.click();
        supp_autre.click();
        button_OK.click();
    }

}