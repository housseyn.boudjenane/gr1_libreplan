import Outils.PageOutils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static org.junit.jupiter.api.Assertions.*;

public class PageCriteres extends PageOutils {

    public PageCriteres(WebDriver driver) {
        super(driver);
        myPageFactory(driver, this);
    }

    // WebElement représentant l'onglet "Ressources"
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Ressources')]")
    WebElement onglet_Ressources;

    // WebElement représentant l'item "Criteres"
    @FindBy(xpath = "//a[@href='/libreplan/resources/criterions/criterions.zul']")
    WebElement item_criteres;

    // WebElement représentant le titre de la page "Types de critères Liste"
    @FindBy(xpath = "//div[@class='z-window-embedded-header' and contains(text(), 'Types de critères Liste')]")
    WebElement title_citere;

    // WebElement représentant le titre de colonne "Nom"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Nom')]")
    WebElement criteres_nom;

    // WebElement représentant le titre de colonne "Type"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Type')]")
    WebElement criteres_type;

    // WebElement représentant le titre de colonne "Code"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Code')]")
    WebElement criteres_code;

    // WebElement représentant le titre de colonne "Activé"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Activé')]")
    WebElement criteres_active;

    // WebElement représentant le titre de colonne "Opérations"
    @FindBy(xpath = "//div[@class='z-column-cnt' and contains(text(), 'Opérations')]")
    WebElement criteres_operations;

    // WebElement représentant le boutton Créer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Créer']")
    WebElement boutton_creer;

    // WebElement représentant le boutton Enregistrer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Enregistrer']")
    WebElement boutton_enregistrer;

    // WebElement représentant le boutton Sauver et continuer
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Sauver et continuer']")
    WebElement boutton_sauver;

    // WebElement représentant le boutton Annuler
    @FindBy(xpath = "//td[@class='z-button-cm' and text() = 'Annuler']")
    WebElement boutton_annuler;

    // WebElement représentant le titre de la page "Créer Type de critère"
    @FindBy(xpath = "//td[@class='z-caption-l' and contains(text(), 'Créer Type de critère')]")
    WebElement title_creer;

    // WebElement représentant l'onglet Modifier
    @FindBy(xpath = "//span[@class='z-tab-text']")
    WebElement onglet_modifier;

    // WebElement représentant le champ Nom
    @FindBy(xpath = "//td/div/input[@class='z-textbox']")
    WebElement field_nom;

    // WebElement représentant le champ Description
    @FindBy(xpath = "//td/div/textarea[@class='z-textbox']")
    WebElement field_description;

    // WebElement représentant la liste déroulante des types
    @FindBy(xpath = "//i[@class='z-combobox-btn']")
    WebElement liste_type;

    // WebElement représentant le type "Participant"
    @FindBy(xpath = "//td[@class='z-comboitem-text' and text() = 'PARTICIPANT']")
    WebElement type_participant;

    // WebElement représentant les champs du tableau de critères
    @FindBy(xpath = "//div[@class='z-grid-body']//span[@class='z-label']")
    WebElement critere_tableau;

    // WebElement représentant le critère "Critère - Test bouton [Annuler]" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Critère - Test bouton [Annuler]']")
    WebElement critere_annuler;

    // WebElement représentant le critère "Critère - Test bouton [Enregistrer]" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Critère - Test bouton [Enregistrer]']")
    WebElement critere_enregistrer;

    // WebElement représentant le message : Type de critère "Critère - Test bouton [Sauver et continuer]" enregistré
    @FindBy(xpath = "//span[text()= 'Type de critère \"Critère - Test bouton [Sauver et continuer]\" enregistré']")
    WebElement message_sauver;

    // WebElement représentant le titre : Modifier Type de critère: Critère - Test bouton [Sauver et continuer]
    @FindBy(xpath = "//td[text() = 'Modifier Type de critère: Critère - Test bouton [Sauver et continuer]']")
    WebElement title_sauver;

    // WebElement représentant le critère "Critère - Test bouton [Sauver et continuer]" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Critère - Test bouton [Sauver et continuer]']")
    WebElement critere_sauver;

    // WebElement représentant le bouton modifier du : Critère - Test bouton [Sauver et continuer]
    @FindBy(xpath = "//span[text()='Critère - Test bouton [Sauver et continuer]']/ancestor::tr//span[@title='Modifier']")
    WebElement boutton_modifier;

    // WebElement représentant le message : Type de critère "Critère - Test bouton [Sauver et continuer]2" enregistré
    @FindBy(xpath = "//span[text()= 'Type de critère \"Critère - Test bouton [Sauver et continuer]2\" enregistré']")
    WebElement message_sauver2;

    // WebElement représentant le titre : Modifier Type de critère: Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//td[text() = 'Modifier Type de critère: Critère - Test bouton [Sauver et continuer]2']")
    WebElement title_sauver2;

    // WebElement représentant le critère "Critère - Test bouton [Sauver et continuer]2" dans le tableau
    @FindBy(xpath = "//span[@class='z-label' and text()= 'Critère - Test bouton [Sauver et continuer]2']")
    WebElement critere_sauver2;

    // WebElement représentant le bouton supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//span[text()='Critère - Test bouton [Sauver et continuer]2']/ancestor::tr//span[@title='Supprimer']")
    WebElement boutton_supprimer;

    // WebElement représentant le message supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//span[text()='Supprimer Type de critère \"Critère - Test bouton [Sauver et continuer]2\". Êtes-vous sûr ?']")
    WebElement message_supprimer;


    // WebElement représentant le boutton OK du message supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//*[@class='z-messagebox-btn z-button']//td[text()='OK']")
    WebElement ok_supprimer;

    // WebElement représentant le boutton Annuler du message supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//*[@class='z-messagebox-btn z-button']//td[text()='Annuler']")
    WebElement annuler_supprimer;

    // WebElement représentant le message : "Type de critère "Critère - Test bouton [Sauver et continuer] 2" supprimé"
    @FindBy(xpath = "//span[text() ='Type de critère \"Critère - Test bouton [Sauver et continuer]2\" supprimé']")
    WebElement message_supprimerOk;

    // WebElement représentant le bouton supprimer de : Critère - Test bouton [Sauver et continuer]2
    @FindBy(xpath = "//span[text()='Critère - Test bouton [Enregistrer]']/ancestor::tr//span[@title='Supprimer']")
    WebElement iconeSup_testEnreg;

    // Méthode pour acceder à la page critéres
    public PageCriteres clickCriteres(WebDriver driver) throws InterruptedException {
        // Bouger la souri sur l'onglet ressources
        moveMouse(onglet_Ressources);

        // Cliquer sur l'item criteres
        click(item_criteres);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        // Assert que les colonnes "Nom" "Type" "Code" "Activé" "Opérations" sont affichés
        assertTrue(criteres_nom.isDisplayed());
        assertTrue(criteres_type.isDisplayed());
        assertTrue(criteres_code.isDisplayed());
        assertTrue(criteres_active.isDisplayed());
        assertTrue(criteres_operations.isDisplayed());

        // Assert que le bouton "Créer" est affiché
        assertTrue(boutton_creer.isDisplayed());

        return PageFactory.initElements(driver, PageCriteres.class);
    }

    // Méthode pour créer un critère
    public void clickCreer (WebDriver driver) throws InterruptedException {
        // Clique sur le bouton creer
        click(boutton_creer);

        // Vérifie que le titre de la page "Créer Type de critère" est affiché
        assertTrue(title_creer.isDisplayed());

        //assert que l'onglet est "Modifier"
        assertEquals(onglet_modifier.getText(), "Modifier");

        // assert que les boutons "Enregistrer" "Sauver et continuer" "Annuler" sont affichés
        assertTrue(boutton_enregistrer.isDisplayed());
        assertTrue(boutton_sauver.isDisplayed());
        assertTrue(boutton_annuler.isDisplayed());
    }

    // Méthode pour créer un critère et annuler
    public void clickAnnuler (WebDriver driver) throws InterruptedException {
        // Saisir le nom
        sendKey(field_nom, "Critère - Test bouton [Annuler]");

        // Saisir le type "participant" dans la liste déroulante
        click(liste_type);
        click(type_participant);

        // Saisir la description
        sendKey(field_description, "Critère - Test bouton [Annuler]");

        // Clique sur le bouton Annuler
        click(boutton_annuler);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        //Assert le critère "Critère - Test bouton [Annuler]" n'est pas présent dans le tableau
        assertFalse(critere_tableau.getText().contains("Critère - Test bouton [Annuler]"));
    }
    // Méthode pour créer un critère et enregistrer
    public void clickEnregistrer (WebDriver driver) throws InterruptedException {
        // Saisir le Nom : Critère - Test bouton [Enregistrer]
        sendKey(field_nom, "Critère - Test bouton [Enregistrer]");

        // Saisir le type "participant" dans la liste déroulante
        click(liste_type);
        click(type_participant);

        // Saisir la description
        sendKey(field_description, "Critère - Test bouton [Enregistrer]");

        // Cliquer sur le bouton "Enregistrer"
        click(boutton_enregistrer);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        // Assert que le critère "Critère - Test bouton [Enregistrer]" est présent dans le tableau
        assertTrue(critere_enregistrer.isDisplayed());
    }

    // Méthode pour créer un critère et Sauver et continuer
    public void clickSauver (WebDriver driver) throws InterruptedException {
        // Saisir le nom
        sendKey(field_nom, "Critère - Test bouton [Sauver et continuer]");

        // Saisir le type "participant" dans la liste déroulante
        click(liste_type);
        click(type_participant);

        // Saisir la description
        sendKey(field_description, "Critère - Test bouton [Sauver et continuer]");

        // Clique sur le bouton sauver
        click(boutton_sauver);

        // Assert que le message "Type de critère "Critère - Test bouton [Sauver et continuer]" enregistré" est affiché
        assertTrue(message_sauver.isDisplayed());

        // Assert que Le titre de la page est : "Modifier Type de critère: Critère - Test bouton [Sauver et continuer]"
        assertTrue(title_sauver.isDisplayed());

        // Clique sur le bouton Annuler
        click(boutton_annuler);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        // Assert que le critère "Critère - Test bouton [Sauver et continuer]" est présent dans le tableau
        assertTrue(critere_sauver.isDisplayed());
    }

    // Méthode pour modifier un critére
    public void clickModifier (WebDriver driver) throws InterruptedException {
        // Clique sur le bouton modifier
        click(boutton_modifier);

        // Assert que Le titre de la page est : "Modifier Type de critère: Critère - Test bouton [Sauver et continuer]"
        assertTrue(title_sauver.isDisplayed());

        // Saisir le nom
        sendKey(field_nom, "Critère - Test bouton [Sauver et continuer]2");

        // Clique sur le bouton Annuler
        click(boutton_annuler);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        // Assert que le critère "Critère - Test bouton [Sauver et continuer]" est présent dans le tableau
        assertTrue(critere_sauver.isDisplayed());

        // Cliquer sur le nom du critère "Critère - Test bouton [Sauver et continuer]" dans le tableau
        click(critere_sauver);

        // Assert que Le titre de la page est : "Modifier Type de critère: Critère - Test bouton [Sauver et continuer]"
        assertTrue(title_sauver.isDisplayed());

        // Saisir le nom
        sendKey(field_nom, "Critère - Test bouton [Sauver et continuer]2");

        // Clique sur le bouton sauver
        click(boutton_sauver);

        // Assert que le message "Type de critère "Critère - Test bouton [Sauver et continuer]2" enregistré" est affiché
        assertTrue(message_sauver2.isDisplayed());

        // Assert que Le titre de la page est : "Modifier Type de critère: Critère - Test bouton [Sauver et continuer]2"
        assertTrue(title_sauver2.isDisplayed());

        // Clique sur le bouton Annuler
        click(boutton_annuler);

        // Assert que le titre de la page "Types de critères Liste" est affiché
        assertTrue(title_citere.isDisplayed());

        // Assert que le critère "Critère - Test bouton [Sauver et continuer]2" est présent dans le tableau
        assertTrue(critere_sauver2.isDisplayed());
    }

    // Méthode pour modifier un critére
    public void clickSupprimer (WebDriver driver) throws InterruptedException {
        // Clique sur le bouton supprimmer
        click(boutton_supprimer);

        // Assert le message suivant "Supprimer Type de critère "Critère - Test bouton [Sauver et continuer] 2". Êtes-vous sûr ?"
        assertTrue(message_supprimer.isDisplayed());

        // Assert les boutons [OK] et [Annuler] dans la pop-up de suppression
        assertTrue(ok_supprimer.isDisplayed());
        assertTrue(annuler_supprimer.isDisplayed());

        // Clique sur le bouton Annuler
        click(annuler_supprimer);

        // Assert que le critère "Critère - Test bouton [Sauver et continuer]2" est présent dans le tableau
        assertTrue(critere_sauver2.isDisplayed());

        // Clique sur le bouton supprimmer
        click(boutton_supprimer);

        // Assert le message suivant "Supprimer Type de critère "Critère - Test bouton [Sauver et continuer] 2". Êtes-vous sûr ?"
        assertTrue(message_supprimer.isDisplayed());

        // Assert les boutons [OK] et [Annuler] dans la pop-up de suppression
        assertTrue(ok_supprimer.isDisplayed());
        assertTrue(annuler_supprimer.isDisplayed());

        // Clique sur le bouton OK
        click(ok_supprimer);

        // Assert Message : "Type de critère "Critère - Test bouton [Sauver et continuer] 2" supprimé"
        assertTrue(message_supprimerOk.isDisplayed());

        // Assert que le critère "Critère - Test bouton [Sauver et continuer] 2" n'est plus présent dans le tableau
        assertFalse(critere_tableau.getText().contains("Critère - Test bouton [Sauver et continuer] 2"));
    }

    // Méthode pour Nettoyage
    public void NettoyageCritere (WebDriver driver) throws InterruptedException {

        // Bouger la souri sur l'onglet ressources
        moveMouse(onglet_Ressources);
        // Cliquer sur l'item criteres
        click(item_criteres);
        // Clique sur le bouton supprimmer
        click(iconeSup_testEnreg);
        // Clique sur le bouton OK
        click(ok_supprimer);
    }
}