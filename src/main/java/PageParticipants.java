import Outils.PageOutils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class PageParticipants extends PageOutils{

    WebDriver driver;

    @FindBy(xpath="//button[contains(@id, 'r-b')]")
    WebElement ongletRessources;

    @FindBy(xpath="//a[contains(@id, 't-a')]")
    WebElement boutonParticipants;

    @FindBy(xpath="//div[contains(@id, 'j4-cap')]")
    WebElement headerListeParticipants;

    @FindBy(xpath="//div[contains(@id, 's5-cave')]")
    WebElement celluleSurnom;

    @FindBy(xpath="//div[contains(@id, 't5-cave')]")
    WebElement cellulePrenom;

    @FindBy(xpath="//div[contains(@id, 'u5-cave')]")
    WebElement celluleID;

    @FindBy(xpath="//div[contains(@id, 'v5-cave')]")
    WebElement celluleCode;

    @FindBy(xpath="//div[contains(@id, 'w5-cave')]")
    WebElement celluleEnFile;

    @FindBy(xpath="//div[contains(@id, 'x5-cave')]")
    WebElement celluleOperations;

    @FindBy(xpath="//span[@title=\"Choisir l'ensemble requis de critères et appuyer sur le bouton filtrer\"]")
    WebElement texteFiltrePar;

    @FindBy(xpath="//input[contains(@id,'4-real')]")
    WebElement champRechercheFiltre;

    @FindBy(xpath="//span[contains(@id,'c5')]")
    WebElement texteDetailsPersonnels;

    @FindBy(xpath="//input[contains(@id,'d5')]")
    WebElement champDetailsPersonnels;

    @FindBy(xpath="//td[contains(@id,'5-cnt')]")
    WebElement boutonPlusDoptions;

    @FindBy(xpath="//td[text()='Filtre']")
    WebElement boutonFiltre;

    @FindBy(xpath="//span[contains(@id,'y5')]")
    WebElement boutonCreer;

    @FindBy(xpath="//td[contains(@id,'6-cnt')]")
    WebElement headerCreerParticipants;

    @FindBy(xpath="//div[contains(@id,'P86')]")
    WebElement blocDonneesDeBase;

    @FindBy(xpath="//input[contains(@id,'k6')]")
    WebElement champCodeNonModifiable;

    @FindBy(xpath="//input[contains(@id,'l6-real')]")
    WebElement caseGenererCode;

    @FindBy(xpath="//input[contains(@id,'o6')]")
    WebElement champPrenomVide;

    @FindBy (xpath="//input[contains(@id,'u6')]")
    WebElement champNomVide;

    @FindBy(xpath = "//input[contains(@id,'x6')]")
    WebElement champIdVide;

    @FindBy(xpath="//select[contains(@id,'17')]")
    WebElement listeDeroulanteParDefaut;

    @FindBy(xpath="//fieldset[contains(@id,'57')]")
    WebElement blocUtilisateurLie;

    @FindBy(xpath="(//input[contains(@id, 'f-real')])[position()=1]")
    WebElement boutonRadioNonLie;

    @FindBy(xpath="(//input[contains(@id, 'f-real')])[position()=2]")
    WebElement boutonRadioUtilisateurExistant;

    @FindBy(xpath="(//input[contains(@id, 'f-real')])[position()=3]")
    WebElement boutonRadioCreerNvlUtilisateur;

    @FindBy(xpath="//td[contains(@class, 'z-button-cm') and text()='Enregistrer']")
    WebElement boutonEnregistrer;

    @FindBy(xpath="//td[contains(@class, 'z-button-cm') and text()='Sauver et continuer']")
    WebElement boutonSauver;

    @FindBy(xpath="//span[contains(@class, 'z-button') and .//td[contains(@class, 'z-button-cm') and text()='Sauver et continuer']]/following-sibling::span//table[contains(@id, 'f-box')]//td[contains(@class, 'z-button-cm') and text()='Annuler']")
    WebElement boutonAnnuler;

    @FindBy(xpath="//tr[.//span[contains(., 'Nom d') and contains(., 'utilisateur')]]/td[2]/div/input[@type='text']")
    WebElement champNomUtilisateur;

    @FindBy(xpath="//tr[td/div/span[contains(text(), 'Mot de passe')]]/td[2]//input[@type='password']")
    WebElement champMotDePasse;

    @FindBy(xpath="//tr[td/div/span[contains(text(), 'Confirmation du mot de passe')]]/td[2]//input[@type='password']\n")
    WebElement champConfirmationMdp;

    @FindBy(xpath="//tr[td/div/span[contains(text(), 'Email')]]/td[2]//input[@type='text']")
    WebElement champEmail;

    @FindBy(xpath="//span[text()='Participant enregistré']")
    WebElement msgValidationParticipant;

    @FindBy(xpath="//span[normalize-space(text())='DU']")
    WebElement nomPresentDansLeTableau;

    @FindBy(xpath="//span[normalize-space(text())='Jean']")
    WebElement prenomPresentDansLeTableau;

    @FindBy(xpath="//span[normalize-space(text())='jdu']")
    WebElement idPresentDansLeTableau;

    @FindBy(xpath="//span[text()='Jean']")
    WebElement assertionJeanPresentTableau;

    @FindBy(xpath="//input[contains(@id,'i5')]")
    WebElement champPeriodeActiveDu;

    @FindBy(xpath="//i[contains(@id,'i5-btn')]")
    WebElement boutonCalendrierActiveDu;

    @FindBy(xpath="//input[contains(@id,'k5')]")
    WebElement champPeriodeActiveAu;

    @FindBy(xpath="//i[contains(@id,'k5-btn')]")
    WebElement boutonCalendrierActiveAu;

    @FindBy(xpath="//select[contains(@id,'5')]")
    WebElement listeDeroulanteType;

    @FindBy(xpath="//table[contains(@id,'5-next')]//button[@type='button' and contains(@class, 'z-paging-next')]")
    WebElement boutonPageSuivante;

    @FindBy(xpath="//table[contains(@id,'5-prev')]//button[@type='button' and contains(@class, 'z-paging-prev')]")
    WebElement boutonPagePrececente;

    @FindBy(xpath="//input[contains(@id, '5-real') and @type='text' and @class='z-paging-inp']")
    WebElement champNumeroDePage;

    @FindBy(xpath="//table[contains(@id,'5-last')]//button[@type='button' and contains(@class, 'z-paging-last')]")
    WebElement boutonDernierePage;

    @FindBy(xpath="//table[contains(@id,'5-first')]//button[@type='button' and contains(@class, 'z-paging-first')]")
    WebElement boutonPremierePage;

    @FindBy(xpath="//a[@class='cerrar_sesion']")
    WebElement seDeconnecter;

    @FindBy(xpath="//input[@id='textfield']")
    WebElement champLogin;

    @FindBy(xpath="//input[@id='textfield2']")
    WebElement champMdp;

    @FindBy(xpath="//input[@id='button']")
    WebElement boutonSeConnecter;

    @FindBy(xpath="//div[contains(@id, '0-cap') and text()='Mon tableau de bord']")
    WebElement assertionTableauDeBord;

    @FindBy(xpath="//tr[.//span[contains(text(), 'DU')] and .//span[contains(text(), 'Jean')]]//img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement iconePoubelleJeanDu;

    @FindBy(xpath="//td[contains(text(),'OK')]")
    WebElement boutonOkSuppression1;

    @FindBy(xpath="//td[contains(text(),'Oui')]")
    WebElement boutonOuiSuppression2;

    @FindBy(xpath="//div[@class='z-grid-body']//span[@class='z-label']")
    WebElement boucleSi;

    @FindBy(xpath="//img[contains(@src,'ico_borrar1.png')]")
    WebElement iconePoubelleTousParticipants;

    private ArrayList<Map<String, String>> listJdd;

    String PATH_CSV = "src/resources/participants.csv";

    private Map<String, String> data;



    // Constructeur de la classe, appelant également le constructeur de la classe parente (PageOutils)
    public PageParticipants(WebDriver driver) {
        super(driver);
        // Initialisation des éléments de la page à l'aide de la méthode myPageFactory
        myPageFactory(driver, this);
    }


    private ArrayList<Map<String, String>> loadCsvJDD() {
        try {
            List<String> lines = Files.readAllLines(Paths.get(PATH_CSV));
            String[] headers = lines.get(0).split(",");
            return lines.stream().skip(1).map(line -> {
                String[] values = line.split(",");
                Map<String, String> row = new HashMap<>();
                for (int i = 0; i < headers.length; i++) {
                    row.put(headers[i], values[i]);
                }
                return row;
            }).collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new RuntimeException("Error reading CSV file", e);
        }
    }



    // Méthode pour créer un participant dans l'application
    public void creerUnParticipant() throws InterruptedException, IOException {

        //*** PAS DE TEST N°2 ***

        // Déplacer la souris sur l'onglet des ressources et cliquer sur le bouton des participants
        moveMouse(ongletRessources);
        click(boutonParticipants);

        // Vérifier que l'en-tête de la liste des participants est correct
        String assertHeader = headerListeParticipants.getText();
        assertEquals("Liste des participants", assertHeader);

        // Assertions pour vérifier les en-têtes de chaque colonne dans le tableau des participants
        String assertSurnom = celluleSurnom.getText();
        assertEquals("Surnom", assertSurnom);

        String assertPrenom = cellulePrenom.getText();
        assertEquals("Prénom", assertPrenom);

        String assertID = celluleID.getText();
        assertEquals("ID", assertID);

        String assertCode = celluleCode.getText();
        assertEquals("Code", assertCode);

        String assertEnFile = celluleEnFile.getText();
        assertEquals("En file", assertEnFile);

        String assertOperations = celluleOperations.getText();
        assertEquals("Opérations", assertOperations);

        // Vérifier la visibilité de divers éléments sur la page
        assertTrue(texteFiltrePar.isDisplayed());
        assertTrue(champRechercheFiltre.isDisplayed());
        assertTrue(texteDetailsPersonnels.isDisplayed());
        assertTrue(champDetailsPersonnels.isDisplayed());
        assertTrue(boutonPlusDoptions.isDisplayed());
        assertTrue(boutonFiltre.isDisplayed());
        assertTrue(boutonCreer.isDisplayed());

        //*** PAS DE TEST N°3 ***

        boolean conditionSi;

        if (boucleSi.getText().contains("Beaulne")) {
            conditionSi = true;
            // Code à exécuter si la condition est vraie
        } else {
            conditionSi = false;
            // Code à exécuter si la condition est fausse
        }

        // Cliquer sur le bouton pour créer un nouveau participant
        click(boutonCreer);
        assertTrue(headerCreerParticipants.isDisplayed());

        //*** PAS DE TEST N°4 ***

        // Vérifier si le champ de code est désactivé
        if (champCodeNonModifiable.getAttribute("disabled") == null) {
            throw new AssertionError("Le champ de saisie est modifiable");
        }

        // Vérifier si la case à cocher pour générer un code est sélectionnée par défaut
        if (!caseGenererCode.isSelected()) {
            throw new AssertionError("La case à cocher n'est pas cochée par défaut");
        }

        // Vérifier que les champs de prénom, nom et ID sont vides par défaut
        String champPrenomReel = champPrenomVide.getText();
        assertTrue(champPrenomReel.isEmpty());

        String champNomReel = champNomVide.getText();
        assertTrue(champNomReel.isEmpty());

        String champIdReel = champIdVide.getText();
        assertTrue(champIdReel.isEmpty());

        // Vérifier la valeur par défaut de la liste déroulante pour le type de ressource
        Select selectListeDeroulante = new Select(listeDeroulanteParDefaut);
        WebElement optionSelectionnee = selectListeDeroulante.getFirstSelectedOption();
        String texteOptionSelectionnee = optionSelectionnee.getText();
        String valeurAttendue = "Ressource normale";
        assertEquals(valeurAttendue, texteOptionSelectionnee, "La valeur par défaut de la liste déroulante n'est pas la bonne");

        // Vérifier si le bouton radio 'Non lié' est sélectionné par défaut
        if (!boutonRadioNonLie.isSelected()) {
            throw new AssertionError("La case à cocher n'est pas cochée par défaut");
        }

        // Vérifier la visibilité de divers éléments de l'interface utilisateur
        assertTrue(boutonRadioUtilisateurExistant.isDisplayed());
        assertTrue(boutonRadioCreerNvlUtilisateur.isDisplayed());
        assertTrue(boutonEnregistrer.isDisplayed());
        assertTrue(boutonSauver.isDisplayed());
        assertTrue(boutonAnnuler.isDisplayed());

        if (!conditionSi){
            listJdd = loadCsvJDD();
            for (int i = 0; i <15; i++) {
                data = listJdd.get(i);
                sendKey(champPrenomVide, data.get("prenom"));
                sendKey(champNomVide, data.get("nom"));
                sendKey(champIdVide, data.get("id"));
                click(boutonEnregistrer);
                click(boutonCreer);
            }
        }



        //*** PAS DE TEST N°5 ***

        // Remplir les champs nécessaires pour créer un nouveau participant
        sendKey(champPrenomVide, "Jean");
        sendKey(champNomVide, "DU");
        sendKey(champIdVide, "jdu");
        click(boutonRadioCreerNvlUtilisateur);
        sendKey(champNomUtilisateur, "jdu");
        sendKey(champMotDePasse, "$jdumdp1");
        sendKey(champConfirmationMdp, "$jdumdp1");
        sendKey(champEmail, "jdu@test.fr");
        click(boutonEnregistrer);

        // Vérifier la visibilité du message de validation et des informations du participant dans le tableau
        Thread.sleep(3000);
        assertTrue(msgValidationParticipant.isDisplayed());
        assertTrue(prenomPresentDansLeTableau.isDisplayed());
        assertTrue(nomPresentDansLeTableau.isDisplayed());
        assertTrue(idPresentDansLeTableau.isDisplayed());

        //*** PAS DE TEST N°6 ***

        // Filtrer le tableau par prénom et vérifier la présence du participant
        sendKey(champDetailsPersonnels, "Jean");
        click(boutonFiltre);
        assertTrue(assertionJeanPresentTableau.isDisplayed());

        //*** PAS DE TEST N°7 ***

        // Vérifier les options avancées du filtre
        click(boutonPlusDoptions);
        assertTrue(champPeriodeActiveDu.isDisplayed());
        assertEquals("", champPeriodeActiveDu.getAttribute("value"));
        assertTrue(champPeriodeActiveAu.isDisplayed());
        assertEquals("", champPeriodeActiveAu.getAttribute("value"));
        assertTrue(boutonCalendrierActiveDu.isDisplayed());
        assertTrue(boutonCalendrierActiveAu.isDisplayed());
        assertTrue(listeDeroulanteType.isDisplayed());

        // Vérifier les options dans la liste déroulante 'Type'
        Select selectType = new Select(listeDeroulanteType);
        String valeurParDefaut = selectType.getFirstSelectedOption().getText();
        assertEquals("Tous", valeurParDefaut);
        List<WebElement> options = selectType.getOptions();
        assertTrue(options.stream().anyMatch(option -> option.getText().equals("Tous")));
        assertTrue(options.stream().anyMatch(option -> option.getText().equals("Ressource en file")));
        assertTrue(options.stream().anyMatch(option -> option.getText().equals("Ressource normale")));
        assertEquals(3, options.size());

        //*** PAS DE TEST N°8/9/10/11 ***

        // Naviguer dans les pages du tableau et vérifier les numéros de page
        moveMouse(ongletRessources);
        click(boutonParticipants);
        click(boutonPageSuivante);
        assertEquals("2", champNumeroDePage.getAttribute("value"));
        click(boutonPagePrececente);
        assertEquals("1", champNumeroDePage.getAttribute("value"));
        click(boutonDernierePage);
        assertEquals("2", champNumeroDePage.getAttribute("value"));
        click(boutonPremierePage);
        assertEquals("1", champNumeroDePage.getAttribute("value"));

        //*** PAS DE TEST N°12 ***

        // Se connecter avec le nouveau participant et vérifier le tableau de bord
        click(seDeconnecter);
        champLogin.clear();
        sendKey(champLogin, "jdu");
        champMdp.clear();
        sendKey(champMdp, "$jdumdp1");
        click(boutonSeConnecter);
        assertEquals("Mon tableau de bord", assertionTableauDeBord.getText());

        // Se reconnecter en tant qu'administrateur et supprimer le compte créé pour le nettoyage
        click(seDeconnecter);
        champLogin.clear();
        sendKey(champLogin, "admin");
        champMdp.clear();
        sendKey(champMdp, "admin");
        click(boutonSeConnecter);
        moveMouse(ongletRessources);
        click(boutonParticipants);

    }

    public void supprimerPerso() throws InterruptedException {
        click(iconePoubelleJeanDu);
        click(boutonOkSuppression1);
        click(boutonOuiSuppression2);
    }

    public void supprimerTousLesParticipants() throws InterruptedException {
        // Localiser tous les éléments de la poubelle par leur icône ou attribut commun
        List<WebElement> iconesPoubelle = driver.findElements(By.xpath("//img[contains(@src,'ico_borrar1.png')]"));

        // Pour chaque élément trouvé, cliquer dessus et confirmer la suppression
        for (WebElement icone : iconesPoubelle) {
            icone.click(); // Cliquer sur l'icône de la poubelle
            click(boutonOkSuppression1); // Cliquer sur le bouton de confirmation 'OK'
            click(boutonOuiSuppression2); // Cliquer sur le bouton de confirmation 'Oui'
            Thread.sleep(1000); // Attendre que l'action de suppression soit terminée

            // Re-localiser les éléments après chaque suppression car le DOM a changé
            iconesPoubelle = driver.findElements(By.xpath("//img[contains(@src,'ico_borrar1.png')]"));
        }
    }

    public void creerJean() throws InterruptedException {

        // Remplir les champs nécessaires pour créer un nouveau participant

        moveMouse(ongletRessources);
        click(boutonParticipants);
        click(boutonCreer);
        sendKey(champPrenomVide, "Jean");
        sendKey(champNomVide, "DU");
        sendKey(champIdVide, "jdu");
        click(boutonRadioCreerNvlUtilisateur);
        sendKey(champNomUtilisateur, "jdu");
        sendKey(champMotDePasse, "$jdumdp1");
        sendKey(champConfirmationMdp, "$jdumdp1");
        sendKey(champEmail, "jdu@test.fr");
        click(boutonEnregistrer);
    }

//    public void supprimerTousLesParticipants() throws InterruptedException {
//        // Continue à trouver et supprimer les icônes de la poubelle tant qu'elles existent
//        while (true) {
//            // Tenter de localiser le premier élément de la poubelle
//            List<WebElement> iconesPoubelle = driver.findElements(By.xpath("//img[contains(@src,'ico_borrar1.png')]"));
//
//            // Si aucune icône n'est trouvée, sortir de la boucle
//            if (iconesPoubelle.isEmpty()) {
//                break;
//            }
//
//            // Cliquer sur la première icône trouvée
//            WebElement icone = iconesPoubelle.get(0);
//            icone.click(); // Cliquer sur l'icône de la poubelle
//            click(boutonOkSuppression1); // Cliquer sur le bouton de confirmation 'OK'
//            click(boutonOuiSuppression2); // Cliquer sur le bouton de confirmation 'Oui'
//            Thread.sleep(1000); // Attendre que l'action de suppression soit terminée
//        }
//    }

}