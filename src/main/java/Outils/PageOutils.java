package Outils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static Outils.SeleniumTools.getWeName;

public class PageOutils {

    private static final List<Object> pageObjectList = new ArrayList<>();
    private static final Pattern PATTERN_LINE_BREAK = Pattern.compile("[\\n\\r]");
    private static final Logger LOGGER = LogManager.getLogger(SeleniumTools.class);
    protected WebDriver driver;

    // Constructeur de la classe PageOutils
    public PageOutils(WebDriver driver) {
        this.driver = driver;
    }

    // Méthode pour lire un fichier CSV et retourner une liste
    public ArrayList<Map<String, String>> inputCSV(String inputFile) throws IOException {
        // Liste pour stocker les objets Map représentant les lignes du fichier CSV
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        // Lire les lignes du fichier CSV et les diviser en tableaux de valeurs
        List<String[]> list = Files.lines(Paths.get(inputFile))
                .map(line -> line.split("\\\\r\\\\n"))
                .collect(Collectors.toList());
        // Parcourir les lignes à partir de la deuxième (index 1), car la première ligne contient les titres
        for (int j = 1; j < list.size(); j++) {
            // Créer un objet Map pour stocker les paires clé-valeur de chaque ligne
            Map<String, String> jdd = new HashMap<>();
            // Récupérer les titres de la première ligne
            String[] titres = list.get(0)[0].split(",");
            // Récupérer les valeurs de la ligne actuelle
            String[] val = list.get(j)[0].split(",");
            // Associer chaque titre à sa valeur correspondante et ajouter à l'objet Map
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            // Ajouter l'objet Map à la liste
            listJDD.add(jdd);
        }
        // Retourner la liste des objets Map représentant les données du fichier CSV
        return listJDD;
    }


    // Méthode d'attente générique avec délai personnalisé
    protected void attendreElement(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    // Méthode pour mettre en surbrillance un élément
    public void highlightMethod(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        // Sauvegarde du style d'origine
        String originalStyle = element.getAttribute("style");

        try {
            // Mise en surbrillance de l'élément
            js.executeScript("arguments[0].scrollIntoView();", element);
            js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

            // Pause facultative pour visualiser la mise en surbrillance
            Thread.sleep(100);

        } catch (InterruptedException e) {
            e.printStackTrace();

        } finally {
            // Restauration du style d'origine, même en cas d'exception
            restoreOriginalStyle(element, originalStyle);
        }
    }

    // Méthode pour restaurer le style d'origine
    private void restoreOriginalStyle(WebElement element, String originalStyle) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, originalStyle);
    }

    // Méthode pour pointer la souris sur un élément
    public void moveMouse(WebElement we) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(we));
        String weName = getWeName(we, nameWebElementLogging(we));
        driver.switchTo().defaultContent();
        Actions bouger = new Actions(driver);
        bouger.moveToElement(we).build().perform();
        LOGGER.info("Move mouse to element: {} with {}", weName);
    }

    // Méthode pour saisir dans un élément
    public void sendKey(WebElement we, String str) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(we));
        String weName = getWeName(we, nameWebElementLogging(we));
        highlightMethod(we);
        we.clear();
        we.sendKeys(str);
        LOGGER.info("Sendkey on element: {} with {}", weName, str);
    }

    // Méthode pour cliquer sur un élément
    public void click(WebElement we) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(we));
        String weName = getWeName(we, nameWebElementLogging(we));
        highlightMethod(we);
        we.click();
        LOGGER.info("Click on element: {}", weName);
    }

    // Autres méthodes de la classe...

    /**
     * Formatage du nom du webElement car si on a un select, ca fait un nom un peu bizarre
     *
     * @param webElement WebElement a intéragir
     * @return nom du webelement pour les logs
     */
    public String nameWebElementLogging(WebElement webElement) {
        String webElementName = getWebElementName(webElement);
        return PATTERN_LINE_BREAK.matcher(webElementName).replaceAll(", ").trim();
    }

    /**
     * Permet de générer le nom du webElement a affiché dans les logs quand il n'y a pas de match avec la méthode getWeName
     *
     * @param element webelement a intéragir
     * @return nom a affiché par défaut
     */
    private static String getWebElementName(WebElement element) {
        String webElementName = element.getText();
        if (webElementName != null && !webElementName.isEmpty()) {
            return webElementName;
        }

        List<String> attributNameList = List.of("name", "title", "id", "value");
        for (String attributName : attributNameList) {
            webElementName = element.getAttribute(attributName);
            if (webElementName != null && !webElementName.isEmpty()) {
                return webElementName;
            }
        }
        return String.format("WebElement: %s", element);
    }

    /**
     * Récupère l'objet portant les noms des variables
     *
     * @param driver     WebDriver utilisé dans le test
     * @param pageObject l'objet portant les @FindBy que l'on veut peupler
     */
    public static void myPageFactory(WebDriver driver, Object pageObject) {
        PageFactory.initElements(driver, pageObject);
        pageObjectList.add(pageObject);
    }

    /**
     * Récupère le nom de la variable du code java
     *
     * @param webElement        le webelement a intéragir
     * @param anotherLogMessage le nom a affiché dans les logs si il n'y a pas de match avec une variable du code java (ex: un Webelement défini avec un driver.findElement)
     * @return le nom a affiché dans les logs quand on fait une action avec le webElement
     */
    public static String getWeName(WebElement webElement, String anotherLogMessage) {
        String webElementName = anotherLogMessage;
        if (!pageObjectList.isEmpty()) {
            var fieldNameList = pageObjectList.stream()
                    .map((Object obj) -> Arrays.stream(obj.getClass().getDeclaredFields())
                            .filter((Field field) -> {
                                try {
                                    boolean isAccess = field.canAccess(obj);
                                    field.setAccessible(true);
                                    boolean isField = field.get(obj) == webElement;
                                    field.setAccessible(isAccess);
                                    return isField;
                                } catch (IllegalAccessException | IllegalArgumentException e) {
                                    return false;
                                }
                            }).collect(Collectors.toList())
                    )
                    .flatMap(List::stream)
                    .map(Field::getName)
                    .collect(Collectors.toList());

            if (fieldNameList.isEmpty()) {
                LOGGER.debug("FieldNameList empty, classList size : {}", pageObjectList.size());
            } else if (fieldNameList.size() > 1) {
                LOGGER.info("FieldNameList size : {}\n{}", fieldNameList.size(), fieldNameList);
            } else {
                webElementName = fieldNameList.get(0);
            }
        } else {
            LOGGER.debug("classList is null");
        }
        return webElementName;
    }

    public static String formatDate(LocalDate date) {
        // Créez un formateur de date avec le modèle souhaité et la locale française
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.FRENCH);

        // Formatez la date en tant que chaîne
        return formatDate.format(date);
    }
}

