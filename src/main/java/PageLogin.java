import Outils.PageOutils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PageLogin extends PageOutils {

    private String username = "admin";
    private String password = "admin";
    String titreAttendu = "LibrePlan: Calendrier";

    // Constructeur de la classe PageLogin
    public PageLogin(WebDriver driver) {
        super(driver);
        myPageFactory(driver, this);
    }

    // WebElement représentant le champ de saisie du nom d'utilisateur sur la page login
    @FindBy (id = "textfield")  //@FindBy(xpath = "//input[@id='textfield']")
    WebElement field_username;

    // WebElement représentant le champ de saisie du mot de passe sur la page login
    @FindBy (id = "textfield2") //@FindBy(xpath = "//input[@id='textfield2']")
    WebElement field_password;

    // WebElement représentant le bouton de connexion sur la page login
    @FindBy (id = "button") //@FindBy(xpath = "//input[@id='button']")
    WebElement bouton_connexion;

    // WebElement représentant l'onglet calendrier
    @FindBy(xpath = "//button[@class='z-menu-btn' and contains(text(), 'Calendrier')]")
    WebElement onglet_calendrier;

    // Méthode de connexion, saisir les informations d'identification et vérifier l'onglet et le titre
    public PageCalendrier SeConnecter(WebDriver driver) throws InterruptedException {

        // Saisir le nom d'utilisateur
        sendKey(field_username, username);

        // Saisir le mot de passe
        sendKey(field_password, password);

        // Clique sur le bouton de connexion
        click(bouton_connexion);

        // Assert que le titre soit celui attendu
        assertEquals(driver.getTitle(), titreAttendu);

        // Vérifie que l'onglet calendrier est affiché
        assertTrue(onglet_calendrier.isDisplayed());

        return PageFactory.initElements(driver, PageCalendrier.class);
    }
}
