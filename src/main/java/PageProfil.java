import Outils.PageOutils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

// La classe PageProfil hérite de PageOutils
public class PageProfil extends PageOutils {

    // WebElement représentant l'onglet Configuration
    @FindBy(css = "button[id$='81-b']")
    WebElement ongletConfiguration;

    // WebElement représentant le bouton profils
    @FindBy(xpath = "//a[contains(@id,'c1-a')]")
    WebElement boutonProfils;

    // WebElement représentant le bouton créer
    @FindBy(xpath ="//td[@class='z-button-cm' and contains(text(), 'Créer')]")
    WebElement boutonCreer;

    // WebElement du titre "Profils Liste" afin de réaliser l'assertion correspondante
    @FindBy(xpath ="//div[contains(@id,'j4-cap')]")
    WebElement assertHeaderProfilsListe;

    // WebElement de la colonne "Nom"
    @FindBy(xpath ="//div[contains(@id,'n4-cave')]")
    WebElement colonneNom;

    // WebElement de la colonne "Actions"
    @FindBy(xpath ="//div[contains(@id,'o4-cave')]")
    WebElement colonneActions;

    // WebElement du titre "Créer Profil" pour réaliser l'assertion correspondante
    @FindBy(xpath ="//td[contains(@id,'x4-cnt')]")
    WebElement assertHeaderCreerProfil;

    // WebElement du champNom
    @FindBy(xpath ="//input[contains(@id, 'a5')]")
    WebElement champNom;

    @FindBy(xpath="//i[contains(@id,'e5-btn')]")
    WebElement boutonListeDeroulante;

    // WebElement pour l'élément "Calendrier" dans la liste déroulante
    @FindBy(xpath="//td[@class='z-comboitem-text' and text()='Calendrier']")
    WebElement calendrierListeDeroulante;

    // WebElement pour l'élément "Canevas" dans la liste déroulante
    @FindBy(xpath="//td[@class='z-comboitem-text' and text()='Canevas']")
    WebElement canevasListeDeroulante;

    // WebElement pour l'élément "Matériels" dans la liste déroulante
    @FindBy(xpath="//td[@class='z-comboitem-text' and text()='Matériels']")
    WebElement materielsListeDeroulante;

    // WebElement pour l'élément "Dépenses" dans la liste déroulante
    @FindBy(xpath="//td[@class='z-comboitem-text' and text()='Dépenses']")
    WebElement depensesListeDeroulante;

    // WebElement du bouton "Ajouter un rôle"
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Ajouter un rôle']")
    WebElement boutonAjouterUnRole;

    // WebElement pour l'assertion du nom du rôle ajouté
    @FindBy(xpath="//span[contains(@id,'s4') and contains(@class, 'z-label')]")
    WebElement assertNomRole;

    // WebElement pour le bouton "Enregistrer"
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Enregistrer']")
    WebElement boutonEnregistrer;

    // WebElement pour le message de validation après l'enregistrement
    @FindBy(xpath="//div[@class='message_INFO']")
    WebElement validationEnregistre;

    // WebElement du bouton "Modifier" du profil que l'on vient de créer
    @FindBy(xpath="//tr[.//span[contains(text(), 'Nom')]]//img[@src='/libreplan/common/img/ico_editar1.png']")
    WebElement boutonModifierProfil;

    //WebElement du message " Profil xxx enregistre"
    @FindBy(xpath="//span[text()='Profil \"test2\" enregistré']")
    WebElement profilModifieEnregistre;

    // WebElement de la poubelle qui a pour position [1]
    @FindBy(xpath="(//tbody[contains(@id,'7')]//img[@src='/libreplan/common/img/ico_borrar1.png'])[1]\n")
    WebElement iconePoubellePosition1;

    //WebElement du body pour pouvoir cliquer ailleurs
    @FindBy(xpath="//body")
    WebElement cliqueBlank;

    //WebElement de la poubelle pour supprimer le profil crée
    @FindBy(xpath="//tr[.//span[text()='test2']]//img[@src='/libreplan/common/img/ico_borrar1.png']")
    WebElement poubelleProfilCree;

    //WebElement du bouton OK pour valider la suppression
    @FindBy(xpath="//td[contains(text(),'OK')]")
    WebElement boutonOkSuppression;

    // Constructeur de la classe, appelant également le constructeur de la classe parente (PageOutils)
    public PageProfil(WebDriver driver) {
        super(driver);
        // Initialisation des éléments de la page à l'aide de la méthode myPageFactory
        myPageFactory(driver, this);
    }

    // Méthode représentant le formulaire de création d'un profil
    public void formulaireCreation (WebDriver driver) throws InterruptedException {
        // Initialisation d'un objet WebDriverWait avec une durée maximale d'attente
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        // Attente de la visibilité de l'élément ongletConfiguration
        wait.until(ExpectedConditions.visibilityOf(ongletConfiguration));
        // Déplacement de la souris sur l'onglet Configuration
        moveMouse(ongletConfiguration);

        // Clique sur le bouton Profils
        click(boutonProfils);

        // Assertion de l'affichage de la page "Profils liste"
        assertEquals("Profils Liste", assertHeaderProfilsListe.getText());

        // Assertion du tableau avec la colonne "Nom" et la colonne "Actions"
        assertEquals("Nom de profil", colonneNom.getText());
        assertEquals("Actions", colonneActions.getText());

        // Cliquer sur le bouton créer
        wait.until(ExpectedConditions.visibilityOf(boutonCreer));
        click(boutonCreer);

        // Assertion de l'affichage "Créer profil"
        assertEquals("Créer Profil", assertHeaderCreerProfil.getText());

        // Assertion du champ "Nom" qui est vide
        String valeurDuChamp = champNom.getText();
        assertTrue(valeurDuChamp.isEmpty());

        // Saisir un nom dans le champ nom
        sendKey(champNom, "Nom");

        // Cliquer sur la liste déroulante
        click(boutonListeDeroulante);

        // Cliquer sur Calendrier dans la liste déroulante
        click(calendrierListeDeroulante);

        // Cliquer sur le bouton ajouter un rôle
        click(boutonAjouterUnRole);

        // Assertion que le rôle a bien été ajouté
        wait.until(ExpectedConditions.visibilityOf(assertNomRole));
        assertEquals("Calendrier", assertNomRole.getText());

//         Assertion que l'icône de poubelle soit affichée
        wait.until(ExpectedConditions.visibilityOf(iconePoubellePosition1));
        assertTrue(iconePoubellePosition1.isDisplayed());

        // Ajouter trois nouveaux rôles
        click(boutonListeDeroulante);
        click(canevasListeDeroulante);
        click(boutonAjouterUnRole);

        click(boutonListeDeroulante);
        click(materielsListeDeroulante);
        click(boutonAjouterUnRole);

        click(boutonListeDeroulante);
        click(depensesListeDeroulante);
        click(boutonAjouterUnRole);

        //Cliquer sur les poubelles pour supprimer l'élément
        click(iconePoubellePosition1);
        /* cliqueBlank clique sur un élément vide afin que le script
        puisse refocus sur les poubelles par la suite */
        click(cliqueBlank);
        click(iconePoubellePosition1);
        click(cliqueBlank);
        click(iconePoubellePosition1);
        click(cliqueBlank);
        click(iconePoubellePosition1);

        // Cliquer sur le bouton enregistrer
        click(boutonEnregistrer);

        // Assertion que l'enregistrement a bien été effectué et que le profil est dans le tableau
        String msgValide = validationEnregistre.getText();
        assertEquals("Profil \"Nom\" enregistré", msgValide);

        /* Modifier le profil en ajoutant
        des rôles et changer son nom en "test2"*/
        click(boutonModifierProfil);

        click(boutonListeDeroulante);
        click(canevasListeDeroulante);
        click(boutonAjouterUnRole);

        click(boutonListeDeroulante);
        click(materielsListeDeroulante);
        click(boutonAjouterUnRole);

        click(boutonListeDeroulante);
        click(depensesListeDeroulante);
        click(boutonAjouterUnRole);
        champNom.clear();
        sendKey(champNom,"test2");
        click(boutonEnregistrer);

        // Vérifier que le profil modifié est bien enregistré
        wait.until(ExpectedConditions.visibilityOf(profilModifieEnregistre));
        assertTrue(profilModifieEnregistre.isDisplayed());

        // Supprimer le profil "test2" créé
        click(poubelleProfilCree);
        click(boutonOkSuppression);

    }
}