import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class PageTest {
    private WebDriver driver;
    private static final String URL = "http://localhost:8080/libreplan/common/layout/login.zul";
    private static final String FIREFOX_DRIVER_PATH = "src/resources/drivers/geckodriver.exe";
    private static final String CHROME_DRIVER_PATH = "src/resources/drivers/chromedriver.exe";

    @Before
    public void setUp() throws InterruptedException {
        // Définir la propriété système pour le pilote Gecko (Firefox)
        System.setProperty("webdriver.gecko.driver", FIREFOX_DRIVER_PATH);
        // Décommentez la ligne suivante si vous souhaitez utiliser le pilote Chrome
//        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);

        // Initialiser le WebDriver en fonction du navigateur choisi
        driver = new FirefoxDriver(); // Utilisation de Firefox
        // Décommentez la ligne suivante si vous souhaitez utiliser le pilote Chrome
//        driver = new ChromeDriver(); // Utilisation de Chrome

        // Accéder à l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();

        // Initialisation de la page PageLogin
        PageLogin pagelogin = PageFactory.initElements(driver, PageLogin.class);

        // Appel de la méthode SeConnecter et récupération de la page suivante (PageCalendrier)
        pagelogin.SeConnecter(driver);
    }

    @Test
    public void CRI_01 () throws InterruptedException {
        // Initialisation de la page PageCriteres
        PageCriteres pagecriteres = PageFactory.initElements(driver, PageCriteres.class);

        // Appel de la méthode clickCriteres
        pagecriteres.clickCriteres(driver);

        // Appel de la méthode clickCréer
        pagecriteres.clickCreer(driver);

        // Appel de la méthode clickAnnuler
        pagecriteres.clickAnnuler(driver);

        // Appel de la méthode clickCréer
        pagecriteres.clickCreer(driver);

        // Appel de la méthode clickCEnregistrer
        pagecriteres.clickEnregistrer(driver);

        // Appel de la méthode clickCréer
        pagecriteres.clickCreer(driver);

        // Appel de la méthode clickCréer
        pagecriteres.clickSauver(driver);

        // Appel de la méthode clickModifier
        pagecriteres.clickModifier(driver);

        // Appel de la méthode clickModifier
        pagecriteres.clickSupprimer(driver);

        // Appel de la méthode de Nettoyage
        pagecriteres.NettoyageCritere(driver);
    }

    @Test
    public void CAL_01 () throws InterruptedException {

        // Initialisation de la page Calendrier
        PageCalendrier pagecalendrier = PageFactory.initElements(driver, PageCalendrier.class);

        // Appel de la méthode creerCalendrier
        pagecalendrier.creerCalendrier(driver);

        // Appel de la méthode Nettoyage Calendrier - test 2
        pagecalendrier.NettoyageCal2(driver);
    }

    @Test
    public void CAL_02 () throws InterruptedException {

        // Initialisation de la page Calendrier
        PageCalendrier pagecalendrier = PageFactory.initElements(driver, PageCalendrier.class);

        // Appel de la méthode ajouterException
        pagecalendrier.ajouterException(driver);

        // Appel de la méthode Nettoyage de l'exception
//        pagecalendrier.NettoyageException(driver);
    }

    @Test
    public void CAL_03 () throws InterruptedException {

        // Initialisation de la page Participants
        PageJoursExceptionnels PageJoursExceptionnels = PageFactory.initElements(driver, PageJoursExceptionnels.class);

        // Appel de la méthode creerJourExceptionnel
        PageJoursExceptionnels.creerJourExceptionnel(driver);

        // Appel des methodes de nettoyage
        PageJoursExceptionnels.NettoyageException(driver);
        PageJoursExceptionnels.NettoyageCal1(driver);
        PageJoursExceptionnels.NettoyageJourExc(driver);

    }

    @Test
    public void GUP_01 () throws InterruptedException {

        // Initialisation de la page PageProfil
        PageProfil pageprofil = PageFactory.initElements(driver, PageProfil.class);

        // Appel de la méthode formulaireCreation
        pageprofil.formulaireCreation(driver);
    }

    @Test
    public void GRE_01() throws InterruptedException, IOException {

        // Initialisation de la page PageParticipants
        PageParticipants pageparticipants = PageFactory.initElements(driver, PageParticipants.class);
        pageparticipants.creerUnParticipant();
        pageparticipants.supprimerPerso();
    }

    @Test
    public void GRE_02() throws  InterruptedException {

        // Initialisation de la page PageParticipants
        PageMachines pagemachines = PageFactory.initElements(driver, PageMachines.class);

        // Appel de la méthode creerMachine
        pagemachines.creerMachine();
    }

    @Test
    public void AVA_01 () throws InterruptedException {

        //Initialisation de la page PageTypeAvancement
        PageTypeAvancement pageavancementtype = PageFactory.initElements(driver, PageTypeAvancement.class);

        // Appel de la méthode creerTypeAvancement
        pageavancementtype.creerTypeAvancement(driver);
    }

//    @After
//    public void tearDown() {
//        driver.quit();
//    }
}