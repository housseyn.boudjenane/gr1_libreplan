import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;


public class PageTest05 {

    private WebDriver driver;
    private WebDriverWait wait;
    private static final String URL = "http://localhost:8080/libreplan/common/layout/login.zul";
    private static final String FIREFOX_DRIVER_PATH = "src/resources/drivers/geckodriver.exe";

    @Before
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", FIREFOX_DRIVER_PATH);
        driver = new FirefoxDriver();
        driver.get(URL);
        driver.manage().window().maximize();
    }

    @Test
    public void PROTA_01() throws InterruptedException {
        // Initialisation de la PageLogin
        PageLogin pagelogin = PageFactory.initElements(driver, PageLogin.class);

        //1. Connexion à l'application - Profil Admin
        // Appel de la méthode SeConnecter et récupération de la page suivante (PageCalendrier)
        PageCalendrier pagecalendrier = pagelogin.SeConnecter(driver);

        // Initialisation de la PageProjet
        PageProjet pageprojet = PageFactory.initElements(driver, PageProjet.class);

        //2. Accéder au formulaire de création d'un projet
        // Appel de la méthode de clic sur l'icône "Créer un nouveau projet" et vérification de la pop-up "Créer un nouveau projet"
        pageprojet.clicCreerProjet(driver);

        //3. Créer un projet - Bouton [Accepter]
        // Appel de la méthode de création d'un projet - Bouton [Accepter] et vérification du projet créé
        pageprojet.creerProjet(driver);

        //4. Vérifier les onglets - menu vertical
        // Appel de la méthode de vérification les onglets - menu vertical
        pageprojet.verifieOrdreMenuVertical();

        //5. Vérifier les onglets - menu horizontal
        // Appel de la méthode de vérification les onglets - menu vertical
        pageprojet.verifieOrdreMenuHorizontal();

        //6. Vérifier les boutons d'enregistrement et d'annulation de l'édition du projet
        // Appel des méthodes de vérification des boutons d'enregistrement et d'annulation de l'édition du projet
        pageprojet.verifieBoutonEnregistrer();
        pageprojet.verifieBoutonAnnuler();

        //7. Vérifier l'utilisation du bouton d'annulation de l'édition du projet (1/4)
        // Appel de la méthode de clic sur le bouton d'annulation de l'édition du projet
        pageprojet.clicAnnulerProjet(driver);

        //8. Vérifier l'utilisation du bouton d'annulation de l'édition du projet (2/4)
        // Appel de la méthode de clic sur le bouton "Annuler" dans la pop-up
        pageprojet.clicAnnulerAnnuler(driver);

        //9. Vérifier l'utilisation du bouton d'annulation de l'édition du projet (3/4)
        // Appel de la méthode de clic sur le bouton d'annulation de l'édition du projet
        pageprojet.clicAnnulerProjet(driver);

        //10. Vérifier l'utilisation du bouton d'annulation de l'édition du projet (4/4)
        // Appel de la méthode de clic sur le bouton "Ok" dans la pop-up
        pageprojet.clicAnnulerOK(driver);

        //11. Vérifier la création du projet
        // Appel de la méthode de clic sur l'item "Projets" dans l'onglet "Calendrier"
        pageprojet.clicProjets(driver);

        //12. Vérifier les informations affichées pour le projet
        // Appel de la méthode de vérification les informations affichées pour le projet
        pageprojet.verifieInfoProjet1();
    }

    @Test
    public void PROTA_02() throws InterruptedException {
        // Initialisation de la PageLogin
        PageLogin pagelogin = PageFactory.initElements(driver, PageLogin.class);

        //1. Connexion à l'application - Profil Admin
        // Appel de la méthode SeConnecter et récupération de la page suivante (PageCalendrier)
        PageCalendrier pagecalendrier = pagelogin.SeConnecter(driver);

        // Initialisation de la PageProjet
        PageProjet pageprojet = PageFactory.initElements(driver, PageProjet.class);

        //2. Accéder à la liste des projets
        // Appel de la méthode de clic sur l'onglet "Liste des projets" dans lel menu vertical à la gauche de la page
        pageprojet.clicListeProjets(driver);

        //3. Accéder à la page d'édition du projet
        // Appel de la méthode de clic sur le nom du projet "PROJET_TEST1" dans la liste des projets
        pageprojet.clicProjet1(driver);

        //4. Vérification affichage de la page d'édition du projet
        // Appel de la méthode de vérification du fil d'ariane affiché
        pageprojet.verifieFilAriane();

        //5. Création d'une nouvelle tâche
        // Appel de la méthode de création d'une nouvelle tâche
        // Tache1-P1 -> Heures = 5)
        pageprojet.creerTache(driver,"Tache1-P1", "5");
        // Appel de la méthode de vérification des en-tetes du tableau dans l'onglet "WBS (tâches)"
        pageprojet.verifieEntetesWbs();
        // Appel de la méthode de l'affichage de la nouvelle ligne de tache
        pageprojet.verifieCellules1WBS();

        //6. Création de plusieurs nouvelles tâches
        // Appel de la méthode de création d'une nouvelle tâche
        // Tache2-P1 -> Heures = 10, Tache3-P1 -> Heures = 20, Tache4-P1 -> Heures = 8
        pageprojet.creerTache(driver,"Tache2-P1", "10");
        pageprojet.creerTache(driver,"Tache3-P1", "20");
        pageprojet.creerTache(driver,"Tache4-P1", "8");
        // Appel de la méthode de vérification de l'affichage des 4 tâches
        pageprojet.verifieOrdreTaches1();

        //7. Modifier l'ordre d'affichage des tâches (1/2)
        // Appel de la méthode de modification l'ordre d'affichage des taches en utilisant la flèche descendre
        pageprojet.modifOrdreTaches1(driver);
        // Appel de la méthode de vérification de l'affichage des 4 tâches
        pageprojet.verifieOrdreTaches2();

        //8. Modifier l'ordre d'affichage des tâches (2/2)
        // Appel de la méthode de modification l'ordre d'affichage des taches en utilisant la flèche montante
        pageprojet.modifOrdreTaches2(driver);
        // Appel de la méthode de vérification de l'affichage des 4 tâches
        pageprojet.verifieOrdreTaches3();

        //9. Renseigner les informations des tâches + enregistrement
        // Appel de la méthode de renseignement les informations des taches
        pageprojet.remplirTaches(driver);
        // Appel de la méthode de clic sur l'icône d'enregistrement du projet (disquette)
        pageprojet.clicEnregistrer(driver);

        //10. Visualisation de la planification du projet
        // Appel de clic sur l'onglet "Planification de projet"
        pageprojet.clicPlanificationProjet(driver);
    }

    @Test
    public void PROTA_03() throws InterruptedException, IOException {
        // Initialisation de la PageLogin
        PageLogin pagelogin = PageFactory.initElements(driver, PageLogin.class);

        //1. Connexion à l'application - Profil Admin
        // Appel de la méthode SeConnecter et récupération de la page suivante (PageCalendrier)
        PageCalendrier pagecalendrier = pagelogin.SeConnecter(driver);

        // Initialisation de la page PageParticipants (Prerequis: Des ressources ont été créés)
        PageParticipants pageparticipants = PageFactory.initElements(driver, PageParticipants.class);
        // Appel de la méthode de création d'un participant
        pageparticipants.creerJean();

        // Initialisation de la PageProjet
        PageProjet pageprojet = PageFactory.initElements(driver, PageProjet.class);

        // Appel de la méthode de clic sur le logo pour revenir à la page de défault
        pageprojet.clicLogo(driver);

        //2. Accéder à la liste des projets
        // Appel de la méthode de clic sur l'onglet "Liste des projets" dans lel menu vertical à la gauche de la page
        pageprojet.clicListeProjets(driver);

        //3. Accéder à la page d'édition du projet
        // Appel de la méthode de clic sur le nom du projet "PROJET_TEST1" dans la liste des projets
        pageprojet.clicProjet1(driver);

        //4. Accéder à la page de planification du projet
        // Appel de la méthode de clic sur l'onglet "Planification de projet"
        pageprojet.clicPlanificationProjet(driver);

        //5. Actions possibles sur une tâche
        // Appel de la méthode de se positionner sur la barre horizontale bleue puis effectuer un clic droit avec la souris
        pageprojet.clicDroitBarre1(driver);
        // Appel de la méthode de vérification l'affichage d'une liste d'actions possibles sur la tâche
        pageprojet.verifieListeActionsTache1();

        //6. Allocation d'une ressource - accès pop-up de saisie
        // Appel de la méthode de clic sur "Allocation de ressources"
        pageprojet.clicAllocation(driver);

        //7. Allocation d'une ressource - affichage liste des ressources
        // Appel de la méthode de clic sur la loupe associée au champ de saisie "Choisir les critères ou les ressources"
        pageprojet.clicLoupeRessource(driver);

        //8. Allocation d'une ressource - sélection d'une ressource
        // Appel de la méthode de clic sur une ressource/1ere ligne dans la liste déroulante associée au champ de saisie "Choisir les critères ou les ressources"
        pageprojet.clicRessource1(driver);

        //9. Allocation d'une ressource - ajout d'une ressource
        // Appel de la méthode de clic sur le bouton "Ajouter"
        pageprojet.clicAjouterRessource(driver);
        // Appel de la méthode de vérification de l'affichage de la ressource dans le tableau Allocations
        //pageprojet.recupererNom1();
        pageprojet.verifieRessource();

        //10. Validation
        // Appel de la méthode de clic sur le bouton "Accepter"
        pageprojet.clicAccepter(driver);

        //11. Vérification de l'allocation de la ressource
        // Appel de la méthode de clic sur l'onglet "Chargement des ressources"
        pageprojet.clicChargementRessources(driver);

        //12. Visualisation de la tâche
        pageprojet.clicIconeNom1(driver);

        //13. Cacher l'affichage de la tâche
        pageprojet.reClicIconeNom1(driver);

        //14. Enregistrement
        // Appel de la méthode de clic sur l'icône d'enregistrement du projet (disquette)
        pageprojet.clicEnregistrer(driver);
    }

    @Test
    public void PROTA_04() throws InterruptedException {
        // Initialisation de la PageLogin
        PageLogin pagelogin = PageFactory.initElements(driver, PageLogin.class);

        //1. Connexion à l'application - Profil Admin
        // Appel de la méthode SeConnecter et récupération de la page suivante (PageCalendrier)
        PageCalendrier pagecalendrier = pagelogin.SeConnecter(driver);

        // Initialisation de la page PageParticipants (Prerequis: Des ressources ont été créés)
        PageParticipants pageparticipants = PageFactory.initElements(driver, PageParticipants.class);

        // Initialisation de la PageProjet
        PageProjet pageprojet = PageFactory.initElements(driver, PageProjet.class);

        //2. Accéder à la liste des projets
        // Appel de la méthode de clic sur l'onglet "Liste des projets" dans lel menu vertical à la gauche de la page
        pageprojet.clicListeProjets(driver);

        //3. Accéder à la page d'édition du projet
        // Appel de la méthode de clic sur le nom du projet "PROJET_TEST1" dans la liste des projets
        pageprojet.clicProjet1(driver);

        //4. Accéder à la page de planification du projet
        // Appel de la méthode de clic sur l'onglet "Planification de projet"
        pageprojet.clicPlanificationProjet(driver);

        //5. Zoom - Année
        // Appel de la méthode de clic sur la valeur "Année"
        pageprojet.clicAnnee(driver);

        //6. Zoom - Trimestre
        // Appel de la méthode de clic sur la valeur "Trimestre"
        pageprojet.clicTrimestre(driver);

        //7. Zoom - Mois
        // Appel de la méthode de clic sur la valeur "Mois"
        pageprojet.clicMois(driver);

        // Appel de la méthode de supprimer de projet
        pageprojet.supprimerProjet(driver);

        // Appel de la méthode de supprimer de participant
        pageparticipants.supprimerPerso1();
    }


    @After
    public void tearDown() {
        driver.quit();
    }
}